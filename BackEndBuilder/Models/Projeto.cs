﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BackEndBuilder.Models {
    public class Projeto {
        public string prefixoTabela { get; set; }
        public string prefixoProcedure { get; set; }
        public string prefixoParametro { get; set; }
        public string sufixoProcedureDel { get; set; }
        public string sufixoProcedureIns { get; set; }
        public string sufixoProcedureUpd { get; set; }
        public string sufixoProcedureSel { get; set; }
        public string infixoProcedureDel { get; set; }
        public string infixoProcedureIns { get; set; }
        public string infixoProcedureUpd { get; set; }
        public string infixoProcedureSel { get; set; }
        public string nomeCampoIdentity { get; set; }
        public bool   nomeDaTabNoCampoIdentity { get; set; }
        public string nomeDoProjeto { get; set; }
        public string nomeMapeamento1 { get; set; }
        public string[] autores { get; set; }
        public string complementoDtoDetalhado { get; set; }
        public string pasta { get; set; }
        public int    contagemLinhas { get; set; }
        public bool   priMaiusculaNasPropriedades { get; set; } //falta fazer
        //estas configurações podem ser deixadas vazias, serão formatadas automaticamente no construtor caso estejam vazias
        public string nomePastaDtos { get; set; } //falta fazer
        public string nomePastaEntidades { get; set; } //falta fazer
        public string nomePastaRepositorios { get; set; } //falta fazer
        public string nomePastaInterfaces { get; set; } //falta fazer
        public string nomePastaAppServices { get; set; } //falta fazer
        public string dataDeCriacao { get; set; } //falta fazer
        public string sufixoDto { get; set; } //falta fazer
        public string sufixoEntidade { get; set; } //falta fazer
        public string sufixoRepositorio { get; set; } //falta fazer
        public string sufixoInterface { get; set; } //falta fazer
        public string sufixoAppService { get; set; } //falta fazer
        public string sufixoController { get; set; } //falta fazer

        public List<API>              apis     = new List<API>();
        public List<Tabela>           tabelas  = new List<Tabela>();
        public List<Schema>           schemas  = new List<Schema>();
        public Dictionary<int, Campo> campos   = new Dictionary<int, Campo>();

        public Projeto(string nomeDoProjeto) {
            this.nomeDoProjeto = nomeDoProjeto;
            nomeMapeamento1    = "";
            prefixoTabela      = "TB_";
            prefixoProcedure   = "PC_";
            prefixoParametro   = "P_";
            infixoProcedureDel = "DEL";
            infixoProcedureIns = "INS";
            infixoProcedureUpd = "UPD";
            infixoProcedureSel = "SEL";
            nomeCampoIdentity  = "COD";
            complementoDtoDetalhado = "Detalhe";
        }

        #region Métodos

        public string CriarProjeto() {
            try {
                CriarPastas();
                FazerValidacoes(1);
                Formatar();
                FazerValidacoes(2);
                CriarPlSqlOwner();
                CriarPlSqlApp();
                CriarPlSqlUser();
                CriarInterfaces();
                CriarEntities();
                CriarDTOs();
                CriarRepositorios();
                CriarAddScope();
                CriarAutoMapper();
                CriarAppServices();
                CriarControllers();
                CriarMapeamentos();
                return $"\nPROJETO GERADO COM SUCESSO \n{GetVolumetria()}\n\nPASTA: {pasta}";
            }
            catch (Exception e) {
                return e.Message;
            }
        }

        public void CriarPastas() {
            string strPasta = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + $"\\BackEndBuilder";
            if (!Directory.Exists(strPasta)) Directory.CreateDirectory(strPasta);
            strPasta += $"\\{nomeDoProjeto}";
            if (!Directory.Exists(strPasta)) Directory.CreateDirectory(strPasta);
            pasta = strPasta + "\\" + DateTime.Now.ToString("yyyy MM dd HH mm ss");
            Directory.CreateDirectory(pasta);
            Directory.CreateDirectory($"{pasta}\\Oracle");
            foreach (API d in apis.Where(x => x.nameSpace != null)) Directory.CreateDirectory($"{pasta}\\{d.nameSpace}");
        }

        public void FazerValidacoes(int tipo) {
            /* FALTA
             * toda tabela deve ter só um campo pk, e pelo menos um campo pksel, ou dá erro de objeto not set to instance
             * não pode ter campos com nomes duplicados na mesma tabela
             * não pode ter tabelas com nomes duplicados
             * não pode ter domínio com nomes ou namespace duplicados
             * um campo não pode fazer referência(fk) a um campo que não existe ou a si próprio, ou a outro campo que está dentro da própria tabela
             * se tem procedure select, a tabela deve ter pelo menos uma chave de select
             * se tem (controller ou appservice) tem que ter pelo menos (pcSel ou pcUpd ou pcIns ou pcDel)
             * config nome procedure deve ter infixo ou sufixo, nunca os dois
             * projeto deve ter pelo menos um autor
             * tabela não pode ter pk de uma tabela de schema diferente
             */
            List<string[]> erros = new List<string[]>() { new string[2] { "Nº", "DESCRIÇÃO DO ERRO" } };
            int contador = 0;
            if (tipo == 1) {
                foreach (API api in apis) {
                    if (api.nameSpace == "") erros.Add(new string[2] { (++contador).ToString(), "UM DOS DOMÍNIOS ESTÁ SEM NAMESPACE" });
                }
                foreach (Tabela t in tabelas) {
                    if (t.GetNomeProc('I').Length > 30) erros.Add(new string[2] { (++contador).ToString(), $"OS PROCEDURES DA TABELA {t.GetNome()} TERÃO MAIS DE 30 CARACTERES NOS NOMES, O ORACLE NÃO VAI ACEITAR" });
                    if (t.GetNome().Length > 30) erros.Add(new string[2] { (++contador).ToString(), $"A TABELA {t.GetNome()} TERÁ MAIS DE 30 CARACTERES NO NOME, O ORACLE NÃO VAI ACEITAR" });
                }
            }
            else if (tipo == 2) {
                foreach (var c in campos) {
                    if (c.Value.nome?.Length > 30) erros.Add(new string[2] { (++contador).ToString(), $"O CAMPO {c.Key} TERÁ MAIS DE 30 CARACTERES NO NOME, O ORACLE NÃO VAI ACEITAR" });
                    if ((c.Value.nome ?? "") == "") erros.Add(new string[2] { (++contador).ToString(), $"O CAMPO {c.Key} ESTÁ SEM NOME" });
                    if ((c.Value.tipo ?? "") == "") erros.Add(new string[2] { (++contador).ToString(), $"O CAMPO {c.Key} ESTÁ SEM TIPO" });
                }
            }
            if (erros.Count > 1) {
                F.StringToArquivo($"{pasta}\\ERROS.html", F.ListToHtml(erros, $"ERROS"));
                throw new Exception("\nNÃO FOI POSSÍVEL CRIAR O PROJETO, POIS FORAM ENCONTRADOS ERROS NA BASE DE DADOS.\nA LISTA COMPLETA DE ERROS FOI SALVA NO ARQUIVO 'ERROS.html'");
            }
        }

        public void Formatar() {
            foreach (var c in campos.Where(x => x.Value.identity)) {
                c.Value.nome = c.Value.tabela.api.projeto.nomeCampoIdentity + (c.Value.tabela.api.projeto.nomeDaTabNoCampoIdentity ? $"_{c.Value.tabela.nome.ToUpper()}" : "");
                c.Value.tipo = "INTEGER";
                c.Value.notNull = true;
                c.Value.pk = true;
                if (c.Value.autoIncrementa == null) c.Value.autoIncrementa = "ALWAYS";
            }
            foreach (var c in campos) {
                if (c.Value.tipoPronto == Campo.TiposProntos.BOOLEAN) {
                    c.Value.check = "#CAMPO IN (0, 1" + (c.Value.notNull ? ")" : ", NULL)");
                    c.Value.tipo = "NUMBER(1, 0)";
                    c.Value.prefixo = Campo.Prefixos.FLG;
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.UFBRASIL) {
                    c.Value.check = "#CAMPO IN ('AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'" + (c.Value.notNull ? ")" : ", NULL)");
                    c.Value.tipo = "VARCHAR2(2)";
                    c.Value.prefixo = Campo.Prefixos.SLG;
                    if (c.Value.nome == null) c.Value.nome = "UF";
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.SEXO) {
                    c.Value.check = "#CAMPO IN ('M', 'F', 'O'" + (c.Value.notNull ? ")" : ", NULL)");
                    c.Value.tipo = "VARCHAR2(1)";
                    c.Value.prefixo = Campo.Prefixos.SLG;
                    if (c.Value.nome == null) c.Value.nome = "SEXO";
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.TELEFONE) {
                    c.Value.tipo = "VARCHAR2(9)";
                    c.Value.prefixo = Campo.Prefixos.NUM;
                    if (c.Value.nome == null) c.Value.nome = "TELEFONE";
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.EMAIL) {
                    c.Value.tipo = "VARCHAR2(255)";
                    c.Value.prefixo = Campo.Prefixos.DES;
                    if (c.Value.nome == null) c.Value.nome = "EMAIL";
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.RAMAL) {
                    c.Value.tipo = "VARCHAR2(4)";
                    c.Value.prefixo = Campo.Prefixos.NUM;
                    if (c.Value.nome == null) c.Value.nome = "RAMAL";
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.DDD) {
                    c.Value.tipo = "VARCHAR2(3)";
                    c.Value.prefixo = Campo.Prefixos.NUM;
                    if (c.Value.nome == null) c.Value.nome = "DDD";
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.DATA) {
                    c.Value.tipo = "DATE";
                    c.Value.prefixo = Campo.Prefixos.DTA;
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.DATAHORA) {
                    c.Value.tipo = "TIMESTAMP";
                    c.Value.prefixo = Campo.Prefixos.DTH;
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.NOME) {
                    c.Value.tipo = "VARCHAR2(400)";
                    c.Value.prefixo = Campo.Prefixos.NOM;
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.CPF) {
                    c.Value.tipo = "VARCHAR2(11)";
                    c.Value.prefixo = Campo.Prefixos.NUM;
                    if (c.Value.nome == null) c.Value.nome = "CPF";
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.CNPJ) {
                    c.Value.tipo = "VARCHAR2(14)";
                    c.Value.prefixo = Campo.Prefixos.NUM;
                    if (c.Value.nome == null) c.Value.nome = "CNPJ";
                }
                else if (c.Value.tipoPronto == Campo.TiposProntos.REAIS) {
                    c.Value.tipo = "NUMBER(16, 4)";
                    c.Value.prefixo = Campo.Prefixos.VLR;
                }
                if (c.Value.prefixo != Campo.Prefixos.VAZIO && (c.Value.nome ?? "") != "") c.Value.nome = $"{c.Value.prefixo}_{c.Value.nome}";
            }
            for (int i = 0; i < 10; i++) {
                foreach (var c in campos) {
                    if (c.Value.fk != null && c.Value.fk.fk == null) {
                        c.Value.tipo = c.Value.fk.tipo;
                        if (c.Value.nome == null && c.Value.fk.nome != null) c.Value.nome = c.Value.fk.nome;
                        if (c.Value.comentario == null && c.Value.fk.comentario != null) c.Value.comentario = c.Value.fk.comentario;
                    }
                }
            }
            foreach (var c in campos.Where(x => x.Value.check != null && x.Value.nome != null)) c.Value.check = c.Value.check.Replace("#CAMPO", c.Value.nome);
        }

        public void CriarPlSqlOwner() {
            foreach (Schema sch in schemas) {
                if (!Directory.Exists($"{pasta}\\Oracle\\{sch.nome}")) Directory.CreateDirectory($"{pasta}\\Oracle\\{sch.nome}");
                string codigo = $"BEGIN \n{DropsOnwer(sch)}{CodigosTabelas(sch)}{CodigosGrants(sch, false)}\nEND;";
                F.StringToArquivo($"{pasta}\\Oracle\\{sch.nome}\\1_plsql_OWNER.sql", codigo, this);
            }
        }

        public void CriarPlSqlApp() {
            foreach (Schema sch in schemas) {
                if (!Directory.Exists($"{pasta}\\Oracle\\{sch.nome}")) Directory.CreateDirectory($"{pasta}\\Oracle\\{sch.nome}");
                string codigo = $"BEGIN -- ********************************************************************************** \n\n{DropsApp(sch)}\n{CodigosSinonimos(sch, false)}\n{CodigosProcedures(sch)}\n\n\n{CodigosGrants(sch)}\n\n\nEND; --*********************************************************************************";
                F.StringToArquivo($"{pasta}\\Oracle\\{sch.nome}\\2_plsql_APP.sql", codigo, this);
            }
        }

        public static string DropsApp(Schema sch) {
            string retorno = "";
            foreach (Tabela t in sch.projeto.tabelas.Where(x => x.schema == sch)) {
                if (t.gerarPCDelete) retorno += t.GetCodDropProc('D');
                if (t.gerarPCInsert) retorno += t.GetCodDropProc('I');
                if (t.gerarPCUpdate) retorno += t.GetCodDropProc('U');
                if (t.gerarPCSelect) retorno += t.GetCodDropProc('S');
                retorno += t.GetCodDropSynon();
            }
            return retorno;
        }

        public static string CodigosTabelas(Schema sch) {
            string retorno = "";
            List<(string tabFilha, string tabMae)> tabsDependentes = RastrearTabsDependentes(sch.projeto);
            for (int i = 0; i < 99 && tabsDependentes.Where(x => x.tabFilha != "" || x.tabMae != "").Any(); i++) {
                foreach (Tabela t in sch.projeto.tabelas) {
                    if (tabsDependentes.Where(x => x.tabFilha == t.nome && x.tabMae == "").Any()) {
                        if (t.schema == sch) {
                            retorno += $"\n    EXECUTE IMMEDIATE '\n    {t.GetCodTabela().Replace("'", "''")}';\n";
                            retorno += t.comentario != null ? $"    EXECUTE IMMEDIATE '{t.GetCodComentario().Replace("'", "''")}';\n" : "";
                            foreach (var c in t.GetCampos().Where(x => x.Value.comentario != null)) retorno += $"    EXECUTE IMMEDIATE '{c.Value.GetCodComentario().Replace("'", "''")}';\n";
                            retorno += "    " + t.GetCodGrantParaOutrosSchemas();
                        }
                        for (int j = 0; j < tabsDependentes.Count; j++) {
                            if (tabsDependentes[j].tabMae == t.nome) tabsDependentes[j] = (tabsDependentes[j].tabFilha, "");
                            if (tabsDependentes[j].tabFilha == t.nome) tabsDependentes[j] = ("", "");
                        }
                    }
                }
            }
            return retorno + "\n";
        }

        public static string CodigosProcedures(Schema sch) {
            string retorno = CodigosProcedures(sch, 'D') + CodigosProcedures(sch, 'U') + CodigosProcedures(sch, 'S') + CodigosProcedures(sch, 'I');
            return retorno;
        }

        public static string CodigosProcedures(Schema sch, char tipoIUDS) {
            string retorno = "";
            if (tipoIUDS == 'D') {
                foreach (Tabela t in sch.projeto.tabelas.Where(t => t.gerarPCDelete && t.schema == sch)) retorno += "\n\n\nEXECUTE IMMEDIATE '\n" + t.GetCodProcDel() + "';";
            }
            else if (tipoIUDS == 'U') {
                foreach (Tabela t in sch.projeto.tabelas.Where(t => t.gerarPCUpdate && t.schema == sch)) retorno += "\n\n\nEXECUTE IMMEDIATE '\n" + t.GetCodProcUpd() + "';";
            }
            else if (tipoIUDS == 'S') {
                foreach (Tabela t in sch.projeto.tabelas.Where(t => t.gerarPCSelect && t.schema == sch)) retorno += "\n\n\nEXECUTE IMMEDIATE '\n" + t.GetCodProcSel() + "';";
            }
            else if (tipoIUDS == 'I') {
                foreach (Tabela t in sch.projeto.tabelas.Where(t => t.gerarPCInsert && t.schema == sch)) retorno += "\n\n\nEXECUTE IMMEDIATE '\n" + t.GetCodProcIns() + "';";
            }
            return retorno; ;
        }

        public static string CodigosGrants(Schema sch, bool dosProcedures = true) {
            string retorno = "";
            if (dosProcedures) {
                foreach (Tabela t in sch.projeto.tabelas.Where(x => x.schema == sch)) {
                    if (t.gerarPCDelete) retorno += t.GetCodGrantProc('D');
                    if (t.gerarPCInsert) retorno += t.GetCodGrantProc('I');
                    if (t.gerarPCUpdate) retorno += t.GetCodGrantProc('U');
                    if (t.gerarPCSelect) retorno += t.GetCodGrantProc('S');
                }
            }
            else {
                foreach (Tabela t in sch.projeto.tabelas.Where(x => x.schema == sch)) retorno += t.GetCodGrant();
            }
            return retorno;
        }

        public static string DropsOnwer(Schema sch) {
            List<(string tabFilha, string tabMae)> tabsDependentes = RastrearTabsDependentes(sch.projeto);
            string codFinal = "";
            for (int i = 0; i < 999 && tabsDependentes.Where(x => x.tabFilha != "" || x.tabMae != "").Any(); i++) { // repetir várias vezes na esperança de que todas as dependências acabem e todas as tabelas recebam drop
                foreach (Tabela t in sch.projeto.tabelas) {
                    if (!tabsDependentes.Where(x => x.tabMae == t.nome).Any() && tabsDependentes.Where(x => x.tabFilha == t.nome).Any()) { //se não é mãe de ninguém, tem que receber o drop
                        if (t.schema == sch) codFinal += $"    {t.GetCodDrop()} \n";
                        for (int j = 0; j < tabsDependentes.Count; j++) { // como era filha, a mãe dela deixou de ser mãe e deve sair da lista
                            if (tabsDependentes[j].tabFilha == t.nome) tabsDependentes[j] = (tabFilha: "", tabMae: "");
                        }
                    }
                }
            }
            return codFinal;
        }

        public static List<(string tabFilha, string tabMae)> RastrearTabsDependentes(Projeto p) {
            List<(string tabFilha, string tabMae)> tabsDependentes = new List<(string, string)>(); // duas colunas: coluna0=nome da tabela com campo fk/filho, coluna1: nome da tabela com campo pai
            foreach (var c in p.campos.Where(x => x.Value.fk != null)) { // adicionando as tabelas que têm dependentes, com if para evitar repetição
                if (!tabsDependentes.Where(x => x.tabFilha == c.Value.tabela.nome && x.tabMae == c.Value.fk.tabela.nome).Any()) tabsDependentes.Add((tabFilha: c.Value.tabela.nome, tabMae: c.Value.fk.tabela.nome));
            }
            foreach (var c in p.campos.Where(x => x.Value.fk == null)) { // adicionando as tabelas que não têm dependentes e ainda não estão na lista
                if (!tabsDependentes.Where(x => x.tabFilha == c.Value.tabela.nome).Any()) tabsDependentes.Add((tabFilha: c.Value.tabela.nome, tabMae: ""));
            }
            return tabsDependentes;
        }

        public void CriarPlSqlUser() {
            foreach (Schema sch in schemas) {
                if (!Directory.Exists($"{pasta}\\Oracle\\{sch.nome}")) Directory.CreateDirectory($"{pasta}\\Oracle\\{sch.nome}");
                string codigo = $"BEGIN \n\n{CodigosSinonimos(sch)}\nEND;";
                F.StringToArquivo($"{pasta}\\Oracle\\{sch.nome}\\3_plsql_USR.sql", codigo, this);
            }
        }

        public string CodigosSinonimos(Schema sch, bool dosprocedures = true) {
            string retorno = "";
            if (dosprocedures) {
                foreach (Tabela t in sch.projeto.tabelas.Where(x => x.schema == sch)) {
                    if (t.gerarPCDelete) retorno += t.GetCodSynonProc('D');
                    if (t.gerarPCInsert) retorno += t.GetCodSynonProc('I');
                    if (t.gerarPCUpdate) retorno += t.GetCodSynonProc('U');
                    if (t.gerarPCSelect) retorno += t.GetCodSynonProc('S');
                }
            }
            else {
                foreach (Tabela t in sch.projeto.tabelas.Where(x => x.schema == sch)) retorno += t.GetCodSynon();
            }
            return retorno;
        }

        public void CriarEntities() {
            foreach (API api in apis) {
                string novaPasta = $"{pasta}\\{api.nameSpace}\\Entities";
                if (!Directory.Exists(novaPasta)) Directory.CreateDirectory(novaPasta);
                foreach (Tabela t in tabelas.Where(x => x.api == api)) F.StringToArquivo($"{novaPasta}\\{t.nome}Entity.cs", t.GetObjEntity(), this);
            }
        }

        public void CriarDTOs() {
            foreach (API api in apis) {
                string novaPasta = $"{pasta}\\{api.nameSpace}\\DTOs";
                if (!Directory.Exists(novaPasta)) Directory.CreateDirectory(novaPasta);
                foreach (Tabela t in tabelas.Where(x => x.api == api)) F.StringToArquivo($"{novaPasta}\\{t.nome}DTO.cs", t.GetObjDto(), this);
            }
        }

        public void CriarAddScope() {
            foreach (API api in apis) {
                string codigo = "services.AddAutoMapper();\n";
                string novaPasta = $"{pasta}\\{api.nameSpace}\\AddScopes";
                if (!Directory.Exists(novaPasta)) Directory.CreateDirectory(novaPasta);
                foreach (Tabela t in tabelas.Where(x => x.api == api)) codigo += t.GetCodAddScope();
                F.StringToArquivo($"{novaPasta}\\AddScope.cs", codigo, this);
            }

            /* FALTA ISSO, DEPOIS DA ÚLTIMA LINHA
             var mappingConfig = new MapperConfiguration(mc => {
                mc.AddProfile(new CadastroAPIProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Cadastro API",
                    Description = "API de Cadastro ASP.NET Core Web API",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Sistemas Internosr",
                        Email = "sistemasinternos@crefisa.com.br",
                        Url = "https://twitter.com/spboyer"
                    },
                    License = new License
                    {
                        Name = "No Defined",
                        Url = "https://example.com/license"
                    }
                });
            });

            dentro do método Configure, no 'IsDevelopment'
            app.UseSwagger();
                app.UseSwaggerUI(c => {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Banco API V1");
                });

             */

        }

        public void CriarAutoMapper() {
            foreach (API api in apis) {
                string codigo = "";
                string novaPasta = $"{pasta}\\{api.nameSpace}\\AutoMapper";
                if (!Directory.Exists(novaPasta)) Directory.CreateDirectory(novaPasta);
                foreach (Tabela t in tabelas.Where(x => x.api == api)) codigo += t.GetCodAutoMap();
                F.StringToArquivo($"{novaPasta}\\AutoMapper.cs", codigo, this);
            }
        }

        public void CriarInterfaces() {
            foreach (API api in apis) {
                string novaPasta = $"{pasta}\\{api.nameSpace}\\Interfaces";
                if (!Directory.Exists(novaPasta)) Directory.CreateDirectory(novaPasta);
                foreach (Tabela t in tabelas.Where(x => x.api == api)) F.StringToArquivo($"{novaPasta}\\I{t.nome}AppService.cs", t.GetOjbInterface(), this);
            }
        }

        public void CriarAppServices() {
            foreach (API api in apis) {
                string novaPasta = $"{pasta}\\{api.nameSpace}\\AppServices";
                if (!Directory.Exists(novaPasta)) Directory.CreateDirectory(novaPasta);
                foreach (Tabela t in tabelas.Where(x => x.api == api)) F.StringToArquivo($"{novaPasta}\\{t.nome}AppService.cs", t.GetOjbAppService(), this);
            }
        }

        public void CriarControllers() {
            foreach (API api in apis) {
                string novaPasta = $"{pasta}\\{api.nameSpace}\\Controllers";
                if (!Directory.Exists(novaPasta)) Directory.CreateDirectory(novaPasta);
                foreach (Tabela t in tabelas.Where(x => x.api == api && x.gerarController)) F.StringToArquivo($"{novaPasta}\\{t.nome}Controller.cs", t.GetObjController(), this);
            }
        }

        public void CriarRepositorios() {
            foreach (API api in apis) {
                string novaPasta = $"{pasta}\\{api.nameSpace}\\Repositories";
                if (!Directory.Exists(novaPasta)) Directory.CreateDirectory(novaPasta);
                foreach (Tabela t in tabelas.Where(x => x.api == api)) F.StringToArquivo($"{novaPasta}\\{t.nome}Repository.cs", t.GetObjRepository(), this);
            }
        }

        public void CriarMapeamentos() {
            List<string[]> lista = new List<string[]>();
            lista.Add(new string[6] { nomeMapeamento1, "TABELA ORACLE", "CAMPO ORACLE", "API", "ENTITY", "PROPERTY" });
            foreach (var c in campos.Where(x => x.Value.mapeamento1 != null).OrderBy(x => x.Value.nome)) {
                for (int i = 0; i < c.Value.mapeamento1.Length; i++) {
                    lista.Add(new string[6] { c.Value.mapeamento1[i], c.Value.tabela.GetNome(), c.Value.nome, c.Value.tabela.api.nameSpace, $"{c.Value.tabela.nome}Entity", c.Value.GetNomePropriedade() });
                }
            }
            F.StringToArquivo($"{pasta}\\Mapa de Campos {nomeMapeamento1}.html", F.ListToHtml(lista, $"Campos {nomeMapeamento1}"));
        }

        public string GetVolumetria() {
            int countTabs = tabelas.Count();
            int countProc = tabelas.Where(x => x.gerarPCDelete).Count() + tabelas.Where(x => x.gerarPCInsert).Count() + tabelas.Where(x => x.gerarPCSelect).Count() + tabelas.Where(x => x.gerarPCUpdate).Count();
            int countAppS = tabelas.Where(x => x.gerarPCDelete || x.gerarPCInsert || x.gerarPCUpdate || x.gerarPCSelect || x.gerarDTOComFilhos).Count();
            int countDtos = countAppS;
            int countInte = countAppS;
            int countEnti = countAppS;
            int countRepo = countAppS;
            int countCont = countAppS;
            int countCamp = campos.Count();
            int countCome = tabelas.Where(x => x.comentario != null).Count() + campos.Where(x => x.Value.comentario != null).Count();
            string retorno = $"{countTabs} TABELAS \n{countProc} PROCEDURES \n{countTabs + countProc} SINÔNIMOS \n{countTabs + countProc} GRANTS \n" +
                             $"{countAppS} APP SERVICES \n{countInte} INTERFACES \n{countDtos} DTOS \n{countEnti} ENTITIES \n{countRepo} REPOSITORIES \n" +
                             $"{countCont} CONTROLLERS \n{countCome} COMENTÁRIOS DE CAMPOS E TABELAS \n{countCamp} CAMPOS \n{contagemLinhas.ToString("#,000")} LINHAS DE CÓDIGO ";
            return retorno;
        }

        #endregion Métodos
    }
}
