﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BackEndBuilder.Models {
    public class Tabela {

        public Tabela(string nome, API api, Schema schema) {
            this.api = api;
            this.schema = schema;
            this.nome = nome;
            gerarController = true;
            api.projeto.tabelas.Add(this);
        }

        public string nome { get; set; }
        public bool gerarPCSelect { get; set; }
        public bool gerarPCInsert { get; set; }
        public bool gerarPCDelete { get; set; }
        public bool gerarPCUpdate { get; set; }
        public bool gerarDTOComFilhos { get; set; }
        public bool gerarController { get; set; }
        public string comentario { get; set; }
        public API api { get; set; }
        public Schema schema { get; set; }


        #region Métodos

        public string GetNome(bool comOwner = false) => ((comOwner ? schema.ownerTabelas + "." : "") + api.projeto.prefixoTabela + api.infixoObjBanco + nome).ToUpper();

        public string GetCodComentario() => $"COMMENT ON TABLE {schema.ownerTabelas}.{GetNome()} IS '{comentario.Replace("'", "")}'";

        public string GetCodTabela() {
            string tbCampos = "";
            string tbFkConstrains = "";
            foreach (var c in GetCampos()) {
                tbCampos += "        " + c.Value.GetCodEConstrains() + ", \n";
                if (c.Value.fk != null) tbFkConstrains += "        " + c.Value.GetFkConstrain() + ", \n";
            }
            if (tbFkConstrains != "") tbFkConstrains = (", \n" + tbFkConstrains).Substring(0, tbFkConstrains.Length);
            string retorno = $"CREATE TABLE {GetNome()} ( \n" +
                             $"{tbCampos.Substring(0, tbCampos.Length - 3)} {tbFkConstrains} " +
                             $"\n    )";
            return retorno;
        }

        public string GetCodGrantProc(char tipoIUDS) => $"EXECUTE IMMEDIATE 'GRANT EXECUTE ON {GetNomeProc(tipoIUDS, true)} TO {schema.ownerUser}'; \n";

        public string GetCodGrant() => $"EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, DELETE, UPDATE ON {GetNome(true)} TO {schema.ownerApps}'; \n";

        public string GetCodGrantParaOutrosSchemas() {
            string retorno = "";
            foreach (Schema sch in api.projeto.schemas.Where(s => s != schema)) {
                retorno += $"EXECUTE IMMEDIATE 'GRANT ALL ON {GetNome(true)} TO {sch.ownerTabelas}'; \n ";
            }
            return retorno;
        }

        public string GetCodDropProc(char tipoIUDS) => $"BEGIN EXECUTE IMMEDIATE 'DROP PROCEDURE {GetNomeProc(tipoIUDS, true)}'; EXCEPTION WHEN OTHERS THEN NULL; END; \n";

        public string GetCodDrop() => $"BEGIN EXECUTE IMMEDIATE 'DROP TABLE {GetNome(true)}'; EXCEPTION WHEN OTHERS THEN NULL; END;";

        public string GetCodSynonProc(char tipoIUDS) => $"EXECUTE IMMEDIATE 'CREATE OR REPLACE SYNONYM {GetNomeProc(tipoIUDS)} FOR {GetNomeProc(tipoIUDS, true)}'; \n";

        public string GetCodSynon() => $"EXECUTE IMMEDIATE 'CREATE OR REPLACE SYNONYM {GetNome()} FOR {GetNome(true)}'; \n";

        public string GetCodDropSynon() => $"BEGIN EXECUTE IMMEDIATE 'DROP SYNONYM {GetNome()}'; EXCEPTION WHEN OTHERS THEN NULL; END; \n";

        public string GetCodProcDel() {
            string nomePc = GetNomeProc('D');
            string nomeTb = GetNome();
            Campo campoPk = GetPk();
            string retorno = $"CREATE OR REPLACE PROCEDURE {nomePc} ({api.projeto.prefixoParametro}{campoPk.nome} {nomeTb}.{campoPk.nome}%TYPE) IS \n" +
                             $"{GetComentarioProc('D')}\n" +
                             $"BEGIN \n" +
                             $"    DELETE FROM {nomeTb} WHERE {campoPk.nome} = {api.projeto.prefixoParametro}{campoPk.nome} AND ROWNUM = 1;\n" +
                             $"    COMMIT;\n" +
                             $"EXCEPTION\n" +
                             $"    WHEN OTHERS THEN \n" +
                             $"        ROLLBACK;\n" +
                             $"        RAISE;\n" +
                             $"END {nomePc};";
            return retorno;
        }

        public string GetCodProcUpd() {
            string nomePc = GetNomeProc('U');
            string nomeTb = GetNome();
            string chave = GetPk().nome;
            string strParametrosEntrada = "";
            string strCamposUpd = "";
            foreach (var c in GetCampos()) {
                strParametrosEntrada += $"{api.projeto.prefixoParametro}{c.Value.nome} {nomeTb}.{c.Value.nome}%TYPE, \n";
                if (!c.Value.identity) strCamposUpd += $"{c.Value.nome} = {api.projeto.prefixoParametro}{c.Value.nome}, \n           ";
            }
            strCamposUpd = strCamposUpd.Substring(0, strCamposUpd.Length - 14);
            string retorno = $"CREATE OR REPLACE PROCEDURE {nomePc} (\n{strParametrosEntrada.Substring(0, strParametrosEntrada.Length - 3)}) IS \n" +
                             $"{GetComentarioProc('U')}\n" +
                             $"BEGIN \n" +
                             $"    UPDATE {nomeTb} \n" +
                             $"       SET {strCamposUpd} \n" +
                             $"     WHERE {chave} = {api.projeto.prefixoParametro}{chave} \n" +
                             $"           AND ROWNUM = 1; \n" +
                             $"    COMMIT;\n" +
                             $"EXCEPTION\n " +
                             $"    WHEN OTHERS THEN \n" +
                             $"        ROLLBACK;\n" +
                             $"        RAISE;\n" +
                             $"END {nomePc};";
            return retorno;
        }

        public string GetCodProcIns() {
            string nomePc = GetNomeProc('I');
            string nomeTb = GetNome();
            string strParametrosEntrada = "";
            string strParametrosInsert = "";
            string strCampos = "";
            Campo campoIdentity = GetIdentity();
            foreach (var c in GetCampos()) {
                strParametrosEntrada += $"{(c.Value == campoIdentity ? "" : api.projeto.prefixoParametro)}{(c.Value == campoIdentity ? "O_SEQUENCE" : c.Value.nome)} {(c.Value == campoIdentity ? "OUT " : "")}{nomeTb}.{c.Value.nome}%TYPE{(c.Value.notNull ? "" : " DEFAULT NULL")}, \n";
                if (!c.Value.identity) strParametrosInsert += $"{api.projeto.prefixoParametro}{c.Value.nome}, ";
                if (!c.Value.identity) strCampos += "  " + c.Value.nome + ", ";
            }
            if (strParametrosInsert.EndsWith(", ")) strParametrosInsert = strParametrosInsert.Substring(0, strParametrosInsert.Length - 2);
            if (strCampos.EndsWith(", ")) strCampos = strCampos.Substring(0, strCampos.Length - 2);
            string retorno = $"CREATE OR REPLACE PROCEDURE {nomePc} (\n{strParametrosEntrada.Substring(0, strParametrosEntrada.Length - 3)}) IS \n" +
                             $"{GetComentarioProc('I')}\n" +
                             $"BEGIN \n" +
                             $"    INSERT INTO {nomeTb} ({strCampos}) \n" +
                             $"         {"".PadRight(nomeTb.Length, ' ')} VALUES ({strParametrosInsert}) \n" +
                             $"      {"".PadRight(nomeTb.Length, ' ')} RETURNING {campoIdentity.nome} INTO O_SEQUENCE; \n" +
                             $"    COMMIT;\n" +
                             $"EXCEPTION\n" +
                             $"    WHEN OTHERS THEN \n" +
                             $"        ROLLBACK;\n" +
                             $"        RAISE;\n" +
                             $"END {nomePc};";
            return retorno;
        }

        public string GetCodProcSel() {
            string nomePc = GetNomeProc('S');
            string nomeTb = GetNome();
            string parametros = "", where = "";
            bool temMaisDeUmSelect = GetCampos().Where(c => c.Value.chaveSel).Count() > 1;
            foreach (var c in GetCampos().Where(x => x.Value.chaveSel)) {
                parametros += $"\n{api.projeto.prefixoParametro}{c.Value.nome} {nomeTb}.{c.Value.nome}%TYPE{(temMaisDeUmSelect ? " DEFAULT NULL" : "")}, ";
                if (temMaisDeUmSelect) {
                    where += $"({c.Value.nome} = {api.projeto.prefixoParametro}{c.Value.nome} OR NVL({api.projeto.prefixoParametro}{c.Value.nome}, 0) = 0) \n            AND ";
                }
                else {
                    where += $"{c.Value.nome} = {api.projeto.prefixoParametro}{c.Value.nome}";
                }
            }
            parametros = parametros.Substring(0, parametros.Length - 2);
            if (temMaisDeUmSelect) where = where.Substring(0, where.Length - 5);
            string chaveSel = GetCampos().Where(c => c.Value.chaveSel).FirstOrDefault().Value.nome;
            string strCampos = "";
            foreach (var c in GetCampos()) strCampos += c.Value.nome + ", ";
            if (strCampos.EndsWith(", ")) strCampos = strCampos.Substring(0, strCampos.Length - 2);
            string retorno = $"CREATE OR REPLACE PROCEDURE {nomePc} ({parametros}, \nRETORNOCURSOR OUT SYS_REFCURSOR) IS \n" +
                             $"{GetComentarioProc('S')}\n" +
                             $"    TYPE T_CURSOR IS REF CURSOR;\n" +
                             $"    C1   T_CURSOR;\n" +
                             $"BEGIN \n" +
                             $"      OPEN C1 FOR \n" +
                             $"    SELECT {strCampos}\n" +
                             $"      FROM {nomeTb}\n" +
                             $"     WHERE {where}; \n" +
                             $"    RETORNOCURSOR := C1; \n" +
                             $"EXCEPTION\n" +
                             $"    WHEN OTHERS THEN RAISE;\n" +
                             $"END {nomePc};";
            return retorno;
        }

        public string GetComentarioProc(char tipoIUDS) {
            string descFuncao = tipoIUDS == 'I' ? "inserir um registro na" :
                                tipoIUDS == 'U' ? "atualizar um registro da" :
                                tipoIUDS == 'D' ? "excluir um registro da" :
                                tipoIUDS == 'S' ? "selecionar um registro da" : "";
            return File.ReadAllText(Environment.CurrentDirectory + "\\ObjModelos\\ComentarioProc.txt")
                       .Replace("[[nomeproc]]", GetNomeProc(tipoIUDS))
                       .Replace("[[autores]]", string.Join(", ", api.projeto.autores))
                       .Replace("[[responsavel]]", api.projeto.autores[0].PadRight(18).Substring(0, 18))
                       .Replace("[[projeto]]", api.projeto.nomeDoProjeto)
                       .Replace("[[data]]", DateTime.Now.ToString("dd/MM/yyyy"))
                       .Replace("[[descricao]]", $"Tem a função de {descFuncao} tabela {GetNome()}");
        }

        public string GetNomeProc(char tipoIUDS, bool comOwner = false) {
            if ("IUDS".IndexOf(tipoIUDS) < 0) return "";
            string infixo = tipoIUDS == 'I' ? api.projeto.infixoProcedureIns ?? "" :
                            tipoIUDS == 'U' ? api.projeto.infixoProcedureUpd ?? "" :
                            tipoIUDS == 'D' ? api.projeto.infixoProcedureDel ?? "" :
                                              api.projeto.infixoProcedureSel ?? "";
            string sufixo = tipoIUDS == 'I' ? api.projeto.sufixoProcedureIns ?? "" :
                            tipoIUDS == 'U' ? api.projeto.sufixoProcedureUpd ?? "" :
                            tipoIUDS == 'D' ? api.projeto.sufixoProcedureDel ?? "" :
                                              api.projeto.sufixoProcedureSel ?? "";
            string retorno = (comOwner ? schema.ownerApps + "." : "") + $"{api.projeto.prefixoProcedure}{api.infixoObjBanco}{infixo}{nome}{sufixo}";
            return retorno.ToUpper();
        }

        public string GetCodAddScope() => $"services.AddScoped<I{nome}AppService, {nome}AppService>();\nservices.AddScoped<IRepositorio<{nome}Entity>, {nome}Repository>();\n";

        public string GetCodAutoMap() => $"CreateMap<{nome}DTO, {nome}Entity>();\nCreateMap<{nome}Entity, {nome}DTO>();\n";

        public string GetObjDto() {
            string codigo = File.ReadAllText(Environment.CurrentDirectory + "\\ObjModelos\\DTO").Replace("[[namespace]]", api.nameSpace).Replace("[[nometabela]]", nome);
            string propriedades = "";
            foreach (var c in GetCampos()) propriedades += "\n        " + c.Value.GetCodPropriedade();
            if (gerarDTOComFilhos) {
                foreach (Tabela tFilha in GetTabsFilhas()) propriedades += $"\n        public IEnumerable<{tFilha.nome}DTO> {F.PriMinuscula(tFilha.nome)}Dto " + "{ get; set; }" + (tFilha.comentario == null ? "" : $" // {tFilha.comentario}");
            }
            return codigo.Replace("[[propriedades]]", propriedades).Replace("[[comentario]]", (comentario == null ? "" : $" // {comentario}"));
        }

        public string GetObjEntity() {
            string codigo = File.ReadAllText(Environment.CurrentDirectory + "\\ObjModelos\\Entity");
            string procedures = "";
            codigo = codigo.Replace("[[namespace]]", api.nameSpace).Replace("[[nometabela]]", nome);
            procedures += gerarPCDelete ? $@"ProcedureExclusao  = ""{GetNomeProc('D')}"", " : "";
            procedures += gerarPCUpdate ? $@"ProcedureAlteracao = ""{GetNomeProc('U')}"", " : "";
            procedures += gerarPCInsert ? $@"ProcedureInclusao  = ""{GetNomeProc('I')}"", " : "";
            procedures += gerarPCSelect ? $@"ProcedureConsulta  = ""{GetNomeProc('S')}"", " : "";
            codigo = codigo.Replace("[[procedures]]", procedures.Substring(0, procedures.Length - 2));
            string propriedades = "";
            foreach (var c in GetCampos()) propriedades += "\n        " + c.Value.GetCodPropriedade(true);
            return codigo.Replace("[[propriedades]]", propriedades).Replace("[[comentario]]", (comentario == null ? "" : $" // {comentario}"));
        }

        public string GetOjbInterface() {
            string codigo = File.ReadAllText(Environment.CurrentDirectory + "\\ObjModelos\\IAppService");
            codigo = codigo.Replace("[[nometabela]]", nome).Replace("[[namespace]]", api.nameSpace).Replace("[[nometabelapriminuscula]]", F.PriMinuscula(nome));
            codigo = gerarPCDelete ? codigo.Replace("[[assinaturadelete]]", $"void Excluir(int chave);") : codigo.Replace("[[assinaturadelete]]", "");
            codigo = gerarPCUpdate ? codigo.Replace("[[assinaturaupdate]]", $"void Alterar({nome}DTO {F.PriMinuscula(nome)}Dto);") : codigo.Replace("[[assinaturaupdate]]", "");
            codigo = gerarPCInsert ? codigo.Replace("[[assinaturainsert]]", $"int Inserir({nome}DTO {F.PriMinuscula(nome)}Dto);") : codigo.Replace("[[assinaturainsert]]", "");
            codigo = gerarPCSelect ? codigo.Replace("[[assinaturaselect]]", $"IEnumerable<{nome}DTO> Consultar({nome}DTO {F.PriMinuscula(nome)}Dto);") : codigo.Replace("[[assinaturaselect]]", "");
            return gerarPCSelect && gerarDTOComFilhos ? codigo.Replace("[[assinaturaDetalheselect]]", $"{nome}DTO {nome}{api.projeto.complementoDtoDetalhado}Consultar({nome}DTO {F.PriMinuscula(nome)}Dto);") : codigo.Replace("[[assinaturaDetalheselect]]", "");
        }

        public string GetOjbAppService() {
            List<Tabela> tabsGerarRepositorio = new List<Tabela>();
            if (gerarDTOComFilhos) {
                foreach (Tabela tFilha in GetTabsFilhas()) tabsGerarRepositorio.Add(tFilha);
            }
            string codigo = File.ReadAllText(Environment.CurrentDirectory + "\\ObjModelos\\AppService").Replace("[[namespace]]", api.nameSpace).Replace("[[nometabela]]", nome);
            string metodoDelete = "", metodoUpdate = "", metodoInsert = "", metodoSelect = "", repositorios = "", metodoDetalheSelect = "";
            if (gerarPCDelete) {
                metodoDelete = "\n        " + $"public void Excluir(int chave) " + "{ \n" +
                                 "        " + $"    var dto = new {nome}DTO() " + "{ " + GetPk().GetNomePropriedade() + " = chave }; \n" +
                                 "        " + $"    var entity = mapper.Map<{nome}Entity>(dto); \n" +
                                 "        " + $"    {F.PriMinuscula(nome)}Repository.Excluir(entity); \n" +
                                 "        " +  "} \n";
            }
            if (gerarPCUpdate) {
                metodoUpdate = "\n        " + $"public void Alterar({nome}DTO dto) " + "{ \n" +
                                 "        " + $"    var entity = mapper.Map<{nome}Entity>(dto); \n" +
                                 "        " + $"    {F.PriMinuscula(nome)}Repository.Atualizar(entity); \n" +
                                 "        " +  "} \n";
            }
            if (gerarPCInsert) {
                metodoInsert = "\n        " + $"public int Inserir({nome}DTO dto) " + "{ \n" +
                                 "        " + $"    var entity = mapper.Map<{nome}Entity>(dto); \n" +
                                 "        " + $"    {F.PriMinuscula(nome)}Repository.Inserir(entity); \n" +
                                 "        " + $"    var novoDto = mapper.Map<{nome}DTO>(entity); \n" +
                                 "        " + $"    return novoDto.{GetIdentity().GetNomePropriedade()}; \n" +
                                 "        " +  "} \n";
            }
            if (gerarPCSelect) {
                metodoSelect = "\n        " + $"public IEnumerable<{nome}DTO> Consultar({nome}DTO dto) " + "{ \n" +
                                 "        " + $"    var entity = mapper.Map<{nome}Entity>(dto); \n" +
                                 "        " + $"    var retornoConsulta = {F.PriMinuscula(nome)}Repository.Consultar(entity); \n" +
                                 "        " + $"    return mapper.Map<IEnumerable<{nome}DTO>>(retornoConsulta); \n" +
                                 "        " +  "} \n";
                if (gerarDTOComFilhos) {
                    Campo pk = GetPk();
                    metodoDetalheSelect = "\n        " + $"public {nome}DTO {nome}{api.projeto.complementoDtoDetalhado}Consultar({nome}DTO {F.PriMinuscula(nome)}Dto) " + "{ \n" +
                                            "        " + $"    var {F.PriMinuscula(nome)}Entity = mapper.Map<{nome}Entity>({F.PriMinuscula(nome)}Dto); \n" +
                                            "        " + $"    {nome}DTO retorno = mapper.Map<{nome}DTO>({F.PriMinuscula(nome)}Repository.Consultar({F.PriMinuscula(nome)}Entity).FirstOrDefault()); \n" +
                                            "        " + $"    \n[[tratamentodasfilhas]]" +
                                            "        " + $"    return retorno; \n" +
                                            "        " +  "} \n";
                    string tratamentoFilhas = "";
                    foreach (Tabela tFilha in tabsGerarRepositorio) {
                        Campo fk = api.projeto.campos.Where(x => x.Value.tabela == tFilha && x.Value.fk != null && x.Value.fk == pk).FirstOrDefault().Value;
                        tratamentoFilhas += "        " + $"    var {F.PriMinuscula(tFilha.nome)}Entity = mapper.Map<{tFilha.nome}Entity>(new {tFilha.nome}DTO() " + "{" + $" {fk.GetNomePropriedade()} = {F.PriMinuscula(nome)}Dto.{pk.GetNomePropriedade()} " + "}); \n" +
                                            "        " + $"    retorno.{F.PriMinuscula(tFilha.nome)}Dto = mapper.Map<IEnumerable<{tFilha.nome}DTO>>({F.PriMinuscula(tFilha.nome)}Repository.Consultar({F.PriMinuscula(tFilha.nome)}Entity)); \n\n";
                    }
                    metodoDetalheSelect = metodoDetalheSelect.Replace("[[tratamentodasfilhas]]", tratamentoFilhas);
                }
            }

            tabsGerarRepositorio.Add(this);
            foreach (Tabela tReposit in tabsGerarRepositorio) {
                repositorios += "\n" + $@"        private {tReposit.nome}Repository {F.PriMinuscula(tReposit.nome)}Repository = new {tReposit.nome}Repository();";
            }

            return codigo.Replace("[[assinaturadelete]]", metodoDelete)
                         .Replace("[[assinaturaupdate]]", metodoUpdate)
                         .Replace("[[assinaturainsert]]", metodoInsert)
                         .Replace("[[assinaturaselect]]", metodoSelect)
                         .Replace("[[repositorios]]", repositorios)
                         .Replace("[[assinaturadetalheselect]]", metodoDetalheSelect);
        }

        public string GetObjController() {
            string codigo = File.ReadAllText(Environment.CurrentDirectory + "\\ObjModelos\\Controller")
                                        .Replace("[[namespace]]", api.nameSpace)
                                        .Replace("[[nometabela]]", nome);
            string metodoDelete = "", metodoUpdate = "", metodoInsert = "", metodoSelect = "", metodoSelectComFilhos = "";
            if (gerarPCDelete) {
                Campo pk = GetPk();
                metodoDelete = "\n        " + $@"[HttpDelete, AllowAnonymous, Route(""{api.versao}/excluir/"")] " + "\n" +
                                 "        " +  $"public async Task<ActionResult> Excluir({pk.GetTipoCSharp()} {pk.GetNomePropriedade()}) " + "{ \n" +
                                 "        " +  $"    try " + "{ \n" +
                                 "        " +  $"        {nome}DTO dto = new {nome}DTO() " + "{" + $" {pk.GetNomePropriedade()} = {pk.GetNomePropriedade()} " + "}; \n" +
                                 "        " +  $"        appService.Excluir({pk.GetNomePropriedade()}); \n" +
                                 "        " +  $"        return Ok(); \n" +
                                 "        " +   "    } \n" +
                                 "        " +   "    catch (Exception ex) { \n" +
                                 "        " + $@"        logger.LogError(ex, ""[{nome}Controller.{nome}Delete] - Erro ao excluir o registro.""" + " + ex.Message + " + @""" | StackTrace = "" + ex.StackTrace, null); " + "\n" +
                                 "        " +   "        return BadRequest(); \n" +
                                 "        " +   "    } \n" +
                                 "        " +   "} \n";
            }
            if (gerarPCUpdate) {
                metodoUpdate = "\n        " + $@"[HttpPost, AllowAnonymous, Route(""{api.versao}/alterar/"")] " + "\n" +
                                 "        " +  $"public async Task<ActionResult> Alterar({nome}DTO model) " + "{ \n" +
                                 "        " +  $"    try " + "{ \n" +
                                 "        " +  $"        appService.Alterar(model);" + " \n" +
                                 "        " +  $"        return Ok();" + " \n" +
                                 "        " +   "    } \n" +
                                 "        " +   "    catch (Exception ex) { \n" +
                                 "        " + $@"        logger.LogError(ex, ""[{nome}Controller.{nome}Update] - Erro ao alterar o registro.""" + " + ex.Message + " + @""" | StackTrace = "" + ex.StackTrace, null); " + "\n" +
                                 "        " +   "        return BadRequest(); \n" +
                                 "        " +   "    } \n" +
                                 "        " +   "} \n";
            }
            if (gerarPCInsert) {
                metodoInsert = "\n        " + $@"[HttpPut, AllowAnonymous, Route(""{api.versao}/inserir/"")] " + "\n" +
                                 "        " +  $"public async Task<ActionResult> Inserir({nome}DTO model) " + "{ \n" +
                                 "        " +  $"    var retorno = new ResponseDTO<int>(); \n" +
                                 "        " +  $"    try " + "{ \n" +
                                 "        " +  $"        retorno.Data = appService.Inserir(model); \n" +
                                 "        " +  $"        retorno.Status = retorno.Data != 0; \n" +
                                 "        " + $@"        retorno.Message = retorno.Status ? ""OK"" : ""Registro não inserido""; " + "\n" +
                                 "        " +  $"        retorno.ExceptionMessage = null; \n" +
                                 "        " +  $"        return Ok(retorno); \n" +
                                 "        " +   "    } \n" +
                                 "        " +   "    catch (Exception ex) { \n" +
                                 "        " + $@"        logger.LogError(ex, ""[{nome}Controller.{nome}Insert] - Erro ao inserir o registro.""" + " + ex.Message + " + @""" | StackTrace = "" + ex.StackTrace, null); " + "\n" +
                                 "        " +   "        retorno.Status = false; \n" +
                                 "        " +   "        retorno.ExceptionMessage = ex.Message; \n" +
                                 "        " +   "        return BadRequest(retorno); \n" +
                                 "        " +   "    } \n" +
                                 "        " +   "} \n";
            }
            if (gerarPCSelect) {
                Campo chaveSel = GetCampos().Where(x => x.Value.chaveSel).FirstOrDefault().Value;
                metodoSelect = "\n        " + $@"/// <summary> " + "\n" +
                                 "        " + $@"/// Consultar {nome} " + "\n" +
                                 "        " + $@"/// </summary> " + "\n" +
                                 "        " + $@"/// <param name=""{chaveSel.GetNomePropriedade()}"">{chaveSel.comentario}</param> " + "\n" +
                                 "        " + $@"/// <returns></returns> " + "\n" +
                                 "        " + $@"[HttpGet, AllowAnonymous, Route(""{api.versao}/consultar/"")] " + "\n" +
                                 "        " +  $"public async Task<ActionResult> Consultar({chaveSel.GetTipoCSharp()} {chaveSel.GetNomePropriedade()}) " + "{ \n" +
                                 "        " +  $"    var retorno = new ResponseDTO<IEnumerable<{nome}DTO>>(); \n" +
                                 "        " +  $"    try " + "{ \n" +
                                 "        " +  $"        {nome}DTO dto = new {nome}DTO " + "{" + $" {chaveSel.GetNomePropriedade()} = {chaveSel.GetNomePropriedade()} " + "}; \n" +
                                 "        " +  $"        retorno.Data = appService.Consultar(dto); \n" +
                                 "        " +  $"        retorno.Status = retorno.Data != null; \n" +
                                 "        " + $@"        retorno.Message = retorno.Status ? ""OK"" : ""Não retornou dados""; " + "\n" +
                                 "        " +  $"        retorno.ExceptionMessage = null; \n" +
                                 "        " +  $"        return Ok(retorno); \n" +
                                 "        " +   "    } \n" +
                                 "        " +   "    catch (Exception ex) { \n" +
                                 "        " + $@"        logger.LogError(ex, ""[{nome}Controller.{nome}Select] - Erro ao buscar os dados.""" + " + ex.Message + " + @""" | StackTrace = "" + ex.StackTrace, null); " + "\n" +
                                 "        " +   "        retorno.Status = false; \n" +
                                 "        " +   "        retorno.ExceptionMessage = ex.Message; \n" +
                                 "        " +   "        return BadRequest(retorno); \n" +
                                 "        " +   "    } \n" +
                                 "        " +   "} \n";
                if (gerarDTOComFilhos) {
                    metodoSelectComFilhos = "\n        " + $@"/// <summary> " + "\n" +
                                              "        " + $@"/// Consultar{api.projeto.complementoDtoDetalhado} {nome} " + "\n" +
                                              "        " + $@"/// </summary> " + "\n" +
                                              "        " + $@"/// <param name=""{chaveSel.GetNomePropriedade()}"">{chaveSel.comentario}</param> " + "\n" +
                                              "        " + $@"/// <returns></returns> " + "\n" +
                                              "        " + $@"[HttpGet, AllowAnonymous, Route(""{api.versao}/consultar{api.projeto.complementoDtoDetalhado.ToLower()}/"")] " + "\n" +
                                              "        " +  $"public async Task<ActionResult> Consultar{api.projeto.complementoDtoDetalhado}({chaveSel.GetTipoCSharp()} {chaveSel.GetNomePropriedade()}) " + "{ \n" +
                                              "        " +  $"    var retorno = new ResponseDTO<{nome}DTO>(); \n" +
                                              "        " +  $"    try " + "{ \n" +
                                              "        " +  $"        {nome}DTO dto = new {nome}DTO " + "{" + $" {chaveSel.GetNomePropriedade()} = {chaveSel.GetNomePropriedade()} " + "}; \n" +
                                              "        " +  $"        retorno.Data = appService.{nome}{api.projeto.complementoDtoDetalhado}Consultar(dto); \n" +
                                              "        " +  $"        retorno.Status = retorno.Data != null; \n" +
                                              "        " + $@"        retorno.Message = retorno.Status ? ""OK"" : ""Não retornou dados""; " + "\n" +
                                              "        " +  $"        retorno.ExceptionMessage = null; \n" +
                                              "        " +  $"        return Ok(retorno); \n" +
                                              "        " +   "    } \n" +
                                              "        " +   "    catch (Exception ex) { \n" +
                                              "        " + $@"        logger.LogError(ex, ""[{nome}Controller.{nome}{api.projeto.complementoDtoDetalhado}Select] - Erro ao buscar os dados.""" + " + ex.Message + " + @""" | StackTrace = "" + ex.StackTrace, null); " + "\n" +
                                              "        " +   "        retorno.Status = false; \n" +
                                              "        " +   "        retorno.ExceptionMessage = ex.Message; \n" +
                                              "        " +   "        return BadRequest(retorno); \n" +
                                              "        " +   "    } \n" +
                                              "        " +   "} \n";
                }
            }
            return codigo.Replace("[[assinaturadelete]]", metodoDelete)
                         .Replace("[[assinaturaupdate]]", metodoUpdate)
                         .Replace("[[assinaturainsert]]", metodoInsert)
                         .Replace("[[assinaturaselect]]", metodoSelect)
                         .Replace("[[assinaturaselectcomfilhos]]", metodoSelectComFilhos);
        }

        public string GetObjRepository() => File.ReadAllText(Environment.CurrentDirectory + "\\ObjModelos\\Repository").Replace("[[namespace]]", api.nameSpace).Replace("[[nometabela]]", nome).Replace("[[connection]]", schema.strConnection);

        public List<Tabela> GetTabsFilhas() {
            List<Tabela> retorno = new List<Tabela>();
            foreach (var c in api.projeto.campos.Where(x => x.Value.fk != null && x.Value.fk.tabela == this && x.Value.fk.pk && x.Value.tabela.api == api)) {
                if (!retorno.Where(x => x == c.Value.tabela).Any()) retorno.Add(c.Value.tabela);
            }
            return retorno;
        }

        public Dictionary<int, Campo> GetCampos() {
            Dictionary<int, Campo> retorno = new Dictionary<int, Campo>();
            foreach (var c in api.projeto.campos.Where(x => x.Value.tabela == this)) retorno.Add(c.Key, c.Value);
            return retorno;
        }

        public Campo GetPk() => GetCampos().Where(x => x.Value.pk).FirstOrDefault().Value;

        public Campo GetIdentity() => GetCampos().Where(x => x.Value.identity).FirstOrDefault().Value;

        #endregion Métodos
    }
}
