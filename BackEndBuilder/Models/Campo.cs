﻿
namespace BackEndBuilder.Models {
    public class Campo {
        public Campo(Tabela tabela) {
            prefixo = Prefixos.VAZIO;
            tipoPronto = TiposProntos.VAZIO;
            this.tabela = tabela;
        }

        public enum Prefixos { COD, DES, DTA, DTH, FLG, NOM, NUM, PCT, QTD, SLG, STA, TPO, VLR, OBS, DIG, VAZIO }
        public enum TiposProntos { TELEFONE, CPF, CNPJ, NOME, REAIS, DATA, DATAHORA, DDD, RAMAL, EMAIL, BOOLEAN, SEXO, UFBRASIL, VAZIO }

        public string nome { get; set; }
        public string tipo { get; set; }
        public bool pk { get; set; }
        public Campo fk { get; set; }
        public bool chaveSel { get; set; }
        public bool notNull { get; set; }
        public bool unique { get; set; }
        public string autoIncrementa { get; set; }
        public string check { get; set; }
        public bool identity { get; set; }
        public Prefixos prefixo { get; set; }
        public TiposProntos tipoPronto { get; set; }
        public Tabela tabela { get; set; }
        public string[] mapeamento1 { get; set; }
        public string comentario { get; set; }

        #region Métodos

        public string GetCodPropriedade(bool comColunaDB = false) {
            //if (campo.fk == null) {
            string retornaPk = identity && tabela.gerarPCInsert ? ", true" : "";
            return (comColunaDB ? $@"[ColunaDB(""{nome}""{retornaPk})] " : "") + $"public {GetTipoCSharp()} {GetNomePropriedade()} " + "{ get; set; }" + (comentario == null ? "" : $" // {comentario}");
            //}
            //else {
            //    return $"public {$"{campo.fk.tabela.nome}DTO"} {$"{F.PriMinuscula(campo.fk.tabela.nome)}Dto"} " + "{ get; set; }";
            //}
        }

        public string GetNomePropriedade() {
            string nomeProp = "";
            foreach (string s in nome.ToLower().Split('_')) nomeProp += F.PriMaiuscula(s);
            return F.PriMinuscula(nomeProp);
        }

        public string GetTipoCSharp() {
            string txt = tipo.ToUpper().Replace(" ", "");
            string retorno = txt.StartsWith("VARCHAR") ? "string" :
                             txt.StartsWith("INTEGER") ? "int" :
                             txt.StartsWith("TIMESTAMP") ? "DateTime" :
                             txt.StartsWith("NUMBER") ? "int" :
                             txt.StartsWith("DATE") ? "DateTime" :
                             txt.StartsWith("BLOB") ? "object" :
                             txt.StartsWith("CLOB") ? "object" : "object";
            if (txt.StartsWith("NUMBER") && !txt.Contains(",0)")) retorno = "float";
            if (txt.EndsWith("NUMBER(1,0)") && check.ToUpper().Replace(" ", "").Contains("IN(0,1")) retorno = "bool";
            return retorno;
        }

        public string GetCodEConstrains() => $"{nome} {tipo} {(identity ? "GENERATED " + autoIncrementa + " AS IDENTITY " : "")}{(pk ? "PRIMARY KEY " : "")}{(unique ? "UNIQUE " : "")}{(notNull ? "NOT NULL " : "")}{(check == null ? "" : "CHECK(" + check + ") ")}";

        public string GetFkConstrain() => $"FOREIGN KEY ({nome}) REFERENCES {fk.tabela.GetNome(true)}({fk.nome})";

        public string GetCodComentario() => $"COMMENT ON COLUMN {tabela.GetNome()}.{nome} IS '{comentario.Replace("'", "")}'";

        #endregion Métodos

    }
}

