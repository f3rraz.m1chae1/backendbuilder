﻿
namespace BackEndBuilder.Models {
    public class Schema {
        public Schema(string nome, Projeto projeto, string ownerTabelas, string ownerApps, string ownerUser, string strConnection) {
            this.nome = nome;
            this.ownerTabelas = ownerTabelas;
            this.ownerApps = ownerApps;
            this.ownerUser = ownerUser;
            this.strConnection = strConnection;
            this.projeto = projeto;
            projeto.schemas.Add(this);
        }
        public string nome { get; set; }
        public string ownerTabelas { get; set; }
        public string ownerApps { get; set; }
        public string ownerUser { get; set; }
        public string strConnection { get; set; }
        public Projeto projeto { get; set; }
    }
}
