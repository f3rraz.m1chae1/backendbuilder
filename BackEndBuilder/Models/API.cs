﻿
namespace BackEndBuilder.Models {
    public class API {

        public API(Projeto projeto, string nameSpace, string infixoObjBanco) {
            this.projeto = projeto;
            projeto.apis.Add(this);
            this.infixoObjBanco = infixoObjBanco;
            this.nameSpace = nameSpace;
        }

        public Projeto projeto { get; set; }
        public string infixoObjBanco { get; set; }
        public string nameSpace { get; set; }
        public string versao { get; set; }

    }
}
