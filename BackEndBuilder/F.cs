﻿using BackEndBuilder.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace BackEndBuilder {

    /// <summary>
    /// FUNÇÕES DE APOIO
    /// </summary>
    public static class F {

        public static string PriMinuscula(string texto) => texto.Substring(0, 1).ToLower() + texto.Substring(1);
        public static string PriMaiuscula(string texto) => texto.Substring(0, 1).ToUpper() + texto.Substring(1);

        public static string ListToHtml(List<string[]> lista, string tituloDaPagina = " ") {
            string retorno = $"<head>\n    <title>{tituloDaPagina}</title>\n</head>\n\n<body>\n<table>\n<thead>\n    <tr>\n        ";
            for (int i = 0; i < lista[0].Length; i++) retorno += $"<th>{lista[0][i]}</th>";
            retorno += "\n    </tr>\n</thead>\n<tbody>\n";
            for (int l = 1; l < lista.Count; l++) {
                string[] mat = lista[l];
                retorno += "    <tr>\n        ";
                for (int i = 0; i < mat.Length; i++) retorno += $"<td>{mat[i]}</td>";
                retorno += "\n    </tr>\n";
            }
            string css = File.ReadAllText($"{Environment.CurrentDirectory}\\ObjModelos\\cssTabela.css");
            return $"{retorno}</tbody>\n</table>\n</body>\n\n\n\n\n\n<style>\n{css}</style>";
        }

        public static void StringToArquivo(string path, string contents, Projeto p = null) {
            if (p != null) p.contagemLinhas += F.StringCountLines(contents);
            File.WriteAllText(path, contents);
        }

        public static int StringCountLines(string str) => str.Split('\n').Length;

    }
}
