﻿using AutoMapper;
using CadastroAPI.AppService;
using CadastroAPI.AutoMapper;
using CadastroAPI.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;

namespace CadastroAPI
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc();
            services.AddAutoMapper();
            services.AddDistributedMemoryCache();
            services.AddScoped< IPessoaAppService               , PessoaAppService>();
            services.AddScoped< IAnaliseAppService              , AnaliseAppService>();
            services.AddScoped< IBeneficioRecebidoInssAppService, BeneficioRecebidoInssAppService>();
            services.AddScoped< ICertificacaoAppService         , CertificacaoAppService>();
            services.AddScoped< IDadoRecebidoInssAppService     , DadoRecebidoInssAppService>();
            services.AddScoped< IPessoaFatcaAppService          , PessoaFatcaAppService>();
            services.AddScoped< ILogAppService                  , LogAppService>();
            services.AddScoped< IPacoteTarifaAppService         , PacoteTarifaAppService>();
            services.AddScoped< IPessoaPepAppService            , PessoaPepAppService>();
            services.AddScoped< IPessoaAppService               , PessoaAppService>();
            services.AddScoped< IPessoaCartaoAppService         , PessoaCartaoAppService>();
            services.AddScoped< IPessoaContaAppService          , PessoaContaAppService>();
            services.AddScoped< IPessoaContaExternaAppService   , PessoaContaExternaAppService>();
            services.AddScoped< IPessoaDocumentoAppService      , PessoaDocumentoAppService>();
            services.AddScoped< IPessoaEmailAppService          , PessoaEmailAppService>();
            services.AddScoped< IPessoaEnderecoAppService       , PessoaEnderecoAppService>();
            services.AddScoped< IPessoaFisicaAppService         , PessoaFisicaAppService>();
            services.AddScoped< IPessoaJuridicaAppService       , PessoaJuridicaAppService>();
            services.AddScoped< IPessoaPatrimonioAppService     , PessoaPatrimonioAppService>();
            services.AddScoped< IPessoaRelativoAppService       , PessoaRelativoAppService>();
            services.AddScoped< IPessoaTelefoneAppService       , PessoaTelefoneAppService>();
            services.AddScoped< IProvaDeVidaAppService          , ProvaDeVidaAppService>();
            services.AddScoped< IPessoaDetalhesAppService       , PessoaDetalhesAppService>();

            var mappingConfig = new MapperConfiguration(mc => {
                mc.AddProfile(new CadastroAPIProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Cadastro API",
                    Description = "API de Cadastro ASP.NET Core Web API",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Sistemas Internosr",
                        Email = "sistemasinternos@crefisa.com.br",
                        Url = "https://twitter.com/spboyer"
                    },
                    License = new License
                    {
                        Name = "No Defined",
                        Url = "https://example.com/license"
                    }
                });
            });

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                

                // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
                // specifying the Swagger JSON endpoint.
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Banco API V1");
                });
            }

            loggerFactory.AddLog4Net();

            app.UseCors(builder =>
                builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials()
                );

            app.UseMvc();
        }
    }

}
