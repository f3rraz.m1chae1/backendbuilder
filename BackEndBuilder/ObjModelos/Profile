﻿using AutoMapper;
using CadastroAPI.DTO;
using CadastroAPI.Entities;
using Crefisa.NC.Comum.DTO.Cadastro;
using Crefisa.NC.Comum.Entities.Cadastro;
using System.Collections.Generic;

namespace CadastroAPI.AutoMapper
{

    /// <summary>
    /// Classe de profile das classes que serão utilizadas na aplicação via AutoMapper.
    /// </summary>
    public class CadastroAPIProfile : Profile
    {

        /// <summary>
        /// Construtor da classe CadastroAPIProfile
        /// </summary>
        public CadastroAPIProfile() {
            // ********************** DE DTO PARA ENTITY **********************************************************
            CreateMap<UsuarioDTO, UsuarioEntity>()
                .ForMember(d => d.CodigoUsusario, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(d => d.NomeUsuario, opt => opt.MapFrom(src => src.Nome));
            CreateMap<LojaDTO, LojaEntity>()
                .ForMember(d => d.CodigoRegional, opt => opt.MapFrom(src => src.Regional != null ? src.Regional.CodigoRegional : 0));
            CreateMap<RegionalDTO, RegionalEntity>()
                .ForMember(d => d.CodigoRegiao, map => map.MapFrom(src => src.Regiao != null ? src.Regiao.CodigoRegiao : 0));
            CreateMap<RegiaoDTO, RegiaoEntity>();
            CreateMap<PessoaDTO, PessoaEntity>();
            CreateMap<AnaliseDTO, AnaliseEntity>();
            CreateMap<BeneficioRecebidoInssDTO, BeneficioRecebidoInssEntity>();
            CreateMap<CertificacaoDTO, CertificacaoEntity>();
            CreateMap<DadoRecebidoInssDTO, DadoRecebidoInssEntity>();
            CreateMap<PessoaFatcaDTO, PessoaFatcaEntity>();
            CreateMap<LogDTO, LogEntity>();
            CreateMap<PacoteTarifaDTO, PacoteTarifaEntity>();
            CreateMap<PessoaPepDTO, PessoaPepEntity>();
            CreateMap<PessoaCartaoDTO, PessoaCartaoEntity>();
            CreateMap<PessoaContaDTO, PessoaContaEntity>();
            CreateMap<PessoaContaExternaDTO, PessoaContaExternaEntity>();
            CreateMap<PessoaDocumentoDTO, PessoaDocumentoEntity>();
            CreateMap<PessoaEmailDTO, PessoaEmailEntity>();
            CreateMap<PessoaEnderecoDTO, PessoaEnderecoEntity>();
            CreateMap<PessoaFisicaDTO, PessoaFisicaEntity>();
            CreateMap<PessoaJuridicaDTO, PessoaJuridicaEntity>();
            CreateMap<PessoaPatrimonioDTO, PessoaPatrimonioEntity>();
            CreateMap<PessoaRelativoDTO, PessoaRelativoEntity>();
            CreateMap<PessoaTelefoneDTO, PessoaTelefoneEntity>();
            CreateMap<ProvaDeVidaDTO, ProvaDeVidaEntity>();



            // ********************** DE ENTITY PARA DTO **********************************************************
            CreateMap<RegionalEntity, RegionalDTO>()
                .ForMember(d => d.Regiao, map => map.MapFrom(src => new RegiaoDTO { CodigoRegiao = src.CodigoRegiao }));
            CreateMap<LojaEntity, LojaDTO>()
                .ForMember(d => d.Regional, map => map.MapFrom(src => new RegionalDTO { CodigoRegional = src.CodigoRegional }));
            CreateMap<UsuarioEntity, UsuarioDTO>()
                .ForMember(d => d.Matricula, opt => opt.MapFrom(src => src.CodigoUsusario))
                .ForMember(d => d.Nome, opt => opt.MapFrom(src => src.NomeUsuario));
            CreateMap<PessoaEntity, PessoaDTO>();
            CreateMap<PerfilEntity, PerfilDTO>();
            CreateMap<RegiaoEntity, RegiaoDTO>();
        }

    }
}
