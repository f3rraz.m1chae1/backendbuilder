﻿using BackEndBuilder.Models;
using System;

namespace BackEndBuilder {
    class Program {

        static void Main() {
            Console.WriteLine("RECRIAR O PROJETO? (S para sim)");
            string resposta = Console.ReadLine();
            if (resposta.ToUpper() == "S") Console.WriteLine(CriarProjeto()); else Console.WriteLine("OPERAÇÃO CANCELADA.");
            Console.WriteLine("\n\nPRESSIONE QUALQUER TECLA PARA SAIR DO PROGRAMA");
            Console.ReadKey();
        }

        public static string CriarProjeto() { // TAB PAROU NO 65, próxima deve ser 66
            Projeto p = new Projeto("INSS");
            p.autores = new string[3] { "Michael Ferraz", "Roberto Costa", "Josué de Paula" };
            p.nomeMapeamento1 = "CNG";
            p.nomeDaTabNoCampoIdentity = true;

            Schema schNfcBanco = new Schema("NFCBCO", p, "OWR_NFCBCO", "APP_NFCBCO", "USR_NFCBCO", "ConnectionNFCBCO");
            Schema schCuc      = new Schema("CUC"   , p, "OWR_CUC"   , "APP_CUC"   , "USR_CUC"   , "ConnectionCUC");

            API apiCad  = new API(p, "CUCAPI"     , "CAD_"  ) { versao = "v1" };
            API apiBco  = new API(p, "NFCBancoAPI", "BCO_"  ) { versao = "v1" };
            API apiInss = new API(p, "InssAPI"    , "INSS_" ) { versao = "v1" };
            API apiLog  = new API(p, "NFCLogAPI"  , ""      ) { versao = "v1" };

            Tabela tb = new Tabela("TipoProfissao", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém tipos de profissões para associa-las às pessoas físicas, parentes, etc"};
            p.campos.Add(1001, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(1002, new Campo(tb) { nome = "TIPOPROFISSAO", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(1003, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("TipoEstadoCivil", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém estados civis para serem associados a pessoas físicas" };
            p.campos.Add(1101, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(1102, new Campo(tb) { nome = "TIPOESTADOCIVIL", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(1103, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("TipoRelacao", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém tipos de relações entre pessoas, por exemplo: filha, esposa, pai, funcionário, advogado, etc" };
            p.campos.Add(1201, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(1202, new Campo(tb) { nome = "TIPORELACAO", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(1203, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("DocumentoTipo", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém tipos de documentos que serão associados a pessoas, solicitações, etc" };
            p.campos.Add(1301, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(1302, new Campo(tb) { nome = "TIPODOC", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(1303, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("DocumentoTipoConfig", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém as configurações possíveis para um documento" };
            p.campos.Add(5501, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(5502, new Campo(tb) { nome = "CONFIGURACAO", tipo = "VARCHAR2(500)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(5503, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("DocumentoConfig", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém as associações entre um tipo de documento, suas configurações possíveis e os valores das configurações" };
            p.campos.Add(5601, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(5602, new Campo(tb) { fk = p.campos[1301], chaveSel = true }); //TIPO DOCUMENTO
            p.campos.Add(5603, new Campo(tb) { fk = p.campos[5501] }); //TIPO CONFIGURAÇÃO
            p.campos.Add(5604, new Campo(tb) { nome = "VALUECONFIG", tipo = "VARCHAR2(4000)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(5605, new Campo(tb) { nome = "INICIOVIGENCIA", tipoPronto = Campo.TiposProntos.DATA });
            p.campos.Add(5606, new Campo(tb) { nome = "FIMVIGENCIA", tipoPronto = Campo.TiposProntos.DATA });
            p.campos.Add(5607, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("DocumentoAcaoTipo", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém as ações possíveis para um documento, ex: digitalizar, imprimir, arquivar, etc." };
            p.campos.Add(5701, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(5702, new Campo(tb) { nome = "PROCESSO", tipo = "VARCHAR2(500)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(5703, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("DocumentoAcao", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém as associações entre um tipo de documento, suas possíveis ações e a ordem em que as ações devem ser executadas" };
            p.campos.Add(5801, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(5802, new Campo(tb) { fk = p.campos[1301], chaveSel = true }); //TIPO DOCUMENTO
            p.campos.Add(5803, new Campo(tb) { fk = p.campos[5701] }); //TIPO AÇÃO
            p.campos.Add(5804, new Campo(tb) { nome = "ORDEM", tipo = "INTEGER", prefixo = Campo.Prefixos.NUM });
            p.campos.Add(5805, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("SolicitaConfigDoc", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém as configurações sobre quais documentos são necessários para cada tipo de solicitação" };
            p.campos.Add(5901, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(5902, new Campo(tb) { nome = "SOLICITACAO", tipo = "VARCHAR2(99)", prefixo = Campo.Prefixos.DES, notNull = true, chaveSel = true });
            p.campos.Add(5903, new Campo(tb) { fk = p.campos[1301], chaveSel = true }); //TIPO DOCUMENTO
            p.campos.Add(5904, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("TipoRecusa", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém os tipos de recusa para uma solicitação" };
            p.campos.Add(1401, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(1402, new Campo(tb) { nome = "TIPORECUSA", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(1403, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("TipoPessoa", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém os tipos de pessoa, ex: Cliente PF, Funcionário PF, Prestador PJ" };
            p.campos.Add(1501, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(1502, new Campo(tb) { nome = "TIPOPESSOA", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(1503, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("TipoConstitJuridica", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém os tipos de constituição de uma empresa: ltda, mei, s/a, etc" };
            p.campos.Add(1601, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(1602, new Campo(tb) { nome = "TIPOCONSTITUICAOJURIDICA", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(1603, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("TipoResidencia", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém tipos de residencia: apartamento, casa, sítio, etc" };
            p.campos.Add(1701, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(1702, new Campo(tb) { nome = "TIPORESIDENCIA", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(1703, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("Pais", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém países para associar a pessoas" };
            p.campos.Add(1801, new Campo(tb) { nome = "PAIS", tipo = "INTEGER", pk = true, notNull = true, chaveSel = true, prefixo = Campo.Prefixos.COD });
            p.campos.Add(1802, new Campo(tb) { nome = "PAIS", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(1803, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("Cidade", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém cidades, para associar a pessoas, documentos, etc" };
            p.campos.Add(1901, new Campo(tb) { identity = true });
            p.campos.Add(1902, new Campo(tb) { nome = "CIDADE", tipo = "VARCHAR2(99)", notNull = true, chaveSel = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(1903, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("Banco", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém bancos, para associar a contas externas de pessoas" };
            p.campos.Add(2001, new Campo(tb) { nome = "BANCO", tipo = "VARCHAR2(5)", notNull = true, pk = true, chaveSel = true, prefixo = Campo.Prefixos.NUM });
            p.campos.Add(2002, new Campo(tb) { nome = "BANCO", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.NOM });
            p.campos.Add(2003, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("TipoGrupoCampoObrig", apiCad, schCuc) { gerarPCSelect = true, comentario = "Contém grupos de sistemas, com isso é possível configurar campos obrigatórios diferentes para grupos de sistemas" };
            p.campos.Add(4401, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(4402, new Campo(tb) { nome = "GRUPOCAMPOBRIG", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(4403, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("TipoMeioPagamento", apiInss, schNfcBanco) { gerarPCSelect = true, comentario = "Contém meios de pagamento" };
            p.campos.Add(4701, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(4702, new Campo(tb) { nome = "MEIOPAGAMENTO", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });

            tb = new Tabela("TipoStatusSolic", apiInss, schNfcBanco) { gerarPCSelect = true, comentario = "Contém tipos possíveis de status de solicitações" };
            p.campos.Add(5201, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(5202, new Campo(tb) { nome = "STATUS", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });

            tb = new Tabela("ProcessoOrigem", apiInss, schNfcBanco) { gerarPCSelect = true, comentario = "Contém tipos de processo que podem originar uma solictação" }; // DEIXA DE EXISTIR?
            p.campos.Add(5301, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(5302, new Campo(tb) { nome = "PROCESSO", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });

            tb = new Tabela("SistemaOrigem", apiInss, schNfcBanco) { gerarPCSelect = true, comentario = "Contém sistemas que podem originar uma solicitação" }; // DEIXA DE EXISTIR?
            p.campos.Add(5401, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(5402, new Campo(tb) { nome = "SISTEMA", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.DES });

            tb = new Tabela("Produto", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém os produtos que podem ser ativados para pessoas físicas ou jurídicas" };
            p.campos.Add(6301, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(6302, new Campo(tb) { nome = "PRODUTO", tipo = "VARCHAR2(99)", prefixo = Campo.Prefixos.DES, notNull = true });
            p.campos.Add(6303, new Campo(tb) { nome = "PRODUTOPAI", tipo = "INTEGER", prefixo = Campo.Prefixos.COD });
            p.campos.Add(6304, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("Pessoa", apiCad, schCuc) { gerarDTOComFilhos = true, gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Dados básicos de pessoas físicas e jurídicas" };
            p.campos.Add(2101, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(2102, new Campo(tb) { nome = "PESSOA", tipoPronto = Campo.TiposProntos.NOME, notNull = true, mapeamento1 = new string[2] { "Nome Cliente", "Nome" } });
            p.campos.Add(2103, new Campo(tb) { nome = "PESSOA", notNull = true, fk = p.campos[1501], prefixo = Campo.Prefixos.TPO });
            p.campos.Add(2104, new Campo(tb) { fk = p.campos[1801], mapeamento1 = new string[2] { "País", "Nacionalidade" } });
            p.campos.Add(2105, new Campo(tb) { fk = p.campos[1901], mapeamento1 = new string[2] { "Cidade", "Local de nascimento" } });
            p.campos.Add(2106, new Campo(tb) { tipoPronto = Campo.TiposProntos.UFBRASIL });
            p.campos.Add(2107, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaJuridica", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Dados de uma pessoa jurídica" };
            p.campos.Add(2201, new Campo(tb) { identity = true });
            p.campos.Add(2202, new Campo(tb) { fk = p.campos[2101], notNull = true, unique = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(2203, new Campo(tb) { tipoPronto = Campo.TiposProntos.CNPJ, chaveSel = true, notNull = true, unique = true });
            p.campos.Add(2204, new Campo(tb) { nome = "RAZAOSOCIAL", tipoPronto = Campo.TiposProntos.NOME, notNull = true });
            p.campos.Add(2205, new Campo(tb) { nome = "ATIVIDADE", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });
            p.campos.Add(2206, new Campo(tb) { nome = "CONSTITUICAOJURIDICA", fk = p.campos[1601], prefixo = Campo.Prefixos.TPO });
            p.campos.Add(2207, new Campo(tb) { nome = "CONSTITUICAO", tipoPronto = Campo.TiposProntos.DATA });
            p.campos.Add(2208, new Campo(tb) { nome = "RECEITA_MES", tipoPronto = Campo.TiposProntos.REAIS });

            tb = new Tabela("PessoaFisica", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Dados de uma pessoa física" };
            p.campos.Add(2301, new Campo(tb) { identity = true });
            p.campos.Add(2302, new Campo(tb) { fk = p.campos[2101], notNull = true, unique = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(2303, new Campo(tb) { tipoPronto = Campo.TiposProntos.CPF, notNull = true, chaveSel = true, unique = true, mapeamento1 = new string[2] { "Número CPF", "CPF" } });
            p.campos.Add(2304, new Campo(tb) { tipoPronto = Campo.TiposProntos.SEXO, mapeamento1 = new string[1] { "Sexo" } });
            p.campos.Add(2305, new Campo(tb) { nome = "NASCIMENTO", tipoPronto = Campo.TiposProntos.DATA, mapeamento1 = new string[1] { "Data nascimento" } });
            p.campos.Add(2306, new Campo(tb) { nome = "PROFISSAO", fk = p.campos[1001], prefixo = Campo.Prefixos.TPO, mapeamento1 = new string[1] { "Profissão" } });
            p.campos.Add(2307, new Campo(tb) { nome = "ESTCIVIL", fk = p.campos[1101], prefixo = Campo.Prefixos.TPO, mapeamento1 = new string[1] { "Estado Civil" } });
            p.campos.Add(2308, new Campo(tb) { nome = "RENDAMES", tipoPronto = Campo.TiposProntos.REAIS, mapeamento1 = new string[1] { "Salário" } });
            p.campos.Add(2309, new Campo(tb) { nome = "BENSEDIREITOS", tipoPronto = Campo.TiposProntos.REAIS, mapeamento1 = new string[1] { "Total em Bens e Direitos" } });
            p.campos.Add(2310, new Campo(tb) { nome = "OUTROSRENDIMENTOS", tipoPronto = Campo.TiposProntos.REAIS, mapeamento1 = new string[2] { "Outros Rendimentos", "Salário/Pro-Labore" } });
            p.campos.Add(2311, new Campo(tb) { nome = "OUTROSRENDIMENTOS", tipo = "VARCHAR2(400)", prefixo = Campo.Prefixos.DES, mapeamento1 = new string[1] { "Outros Rendimentos Especificar" } });
            p.campos.Add(2312, new Campo(tb) { nome = "ENTRADABRASIL", tipoPronto = Campo.TiposProntos.DATA, mapeamento1 = new string[1] { "Data de Entrada no Brasil" } });
            p.campos.Add(2313, new Campo(tb) { nome = "EMPRESA", tipoPronto = Campo.TiposProntos.NOME, mapeamento1 = new string[1] { "Empresa em que trabalha" } });

            tb = new Tabela("PessoaFatca", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCDelete = true, gerarPCUpdate = true, comentario = "Dados de vivência no exterior de uma pessoa física" };
            p.campos.Add(2401, new Campo(tb) { identity = true });
            p.campos.Add(2402, new Campo(tb) { fk = p.campos[2101], notNull = true, unique = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(2403, new Campo(tb) { nome = "ESTUDANTE", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2404, new Campo(tb) { nome = "DIPLOMATA", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2405, new Campo(tb) { nome = "PRESENCA_SUBSTANCIAL", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2406, new Campo(tb) { nome = "ABDICOU_NACIONALIDADE", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2407, new Campo(tb) { nome = "RENUNCIOU", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2408, new Campo(tb) { nome = "POSSUI_GREENCARD", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2409, new Campo(tb) { nome = "CERTIFIC_ABANDONO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2410, new Campo(tb) { nome = "RESIDE_NOS_EUA", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaPep", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCDelete = true, gerarPCUpdate = true, comentario = "Dados de Pessoa Exposta Politicamente" };
            p.campos.Add(2501, new Campo(tb) { identity = true });
            p.campos.Add(2502, new Campo(tb) { fk = p.campos[2101], notNull = true, unique = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(2503, new Campo(tb) { nome = "RESIDE_NOS_EUA", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2504, new Campo(tb) { nome = "FUNCAO", tipo = "VARCHAR2(400)", prefixo = Campo.Prefixos.DES });
            p.campos.Add(2505, new Campo(tb) { nome = "INICIO", tipoPronto = Campo.TiposProntos.DATA, mapeamento1 = new string[1] { "Data de Início do Exercício" } });
            p.campos.Add(2506, new Campo(tb) { nome = "FIM", tipoPronto = Campo.TiposProntos.DATA, mapeamento1 = new string[1] { "Data Fim do Exercício" } });
            p.campos.Add(2507, new Campo(tb) { nome = "EMPRESA", tipoPronto = Campo.TiposProntos.NOME, mapeamento1 = new string[1] { "Empresa/Orgao Publico" } });
            p.campos.Add(2508, new Campo(tb) { nome = "RELACAO_AGENTE_PUBLI", tipoPronto = Campo.TiposProntos.BOOLEAN, mapeamento1 = new string[2] { "Possui Relacionamento com Agente Público", "Exerceu Funcao pública relevante" } });
            p.campos.Add(2509, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaTelefone", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCDelete = true, gerarPCUpdate = true, comentario = "Telefones de pessoas físicas ou jurídicas" };
            p.campos.Add(2601, new Campo(tb) { identity = true });
            p.campos.Add(2602, new Campo(tb) { fk = p.campos[2101], notNull = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(2603, new Campo(tb) { tipoPronto = Campo.TiposProntos.DDD, notNull = true, mapeamento1 = new string[2] { "DDD Telefone Fixo", "DDD Telefone Celular" } });
            p.campos.Add(2604, new Campo(tb) { tipoPronto = Campo.TiposProntos.TELEFONE, notNull = true, mapeamento1 = new string[2] { "Número Telefone Fixo", "Número Telefone Celular" } });
            p.campos.Add(2605, new Campo(tb) { fk = p.campos[1801] });
            p.campos.Add(2606, new Campo(tb) { tipoPronto = Campo.TiposProntos.RAMAL });
            p.campos.Add(2607, new Campo(tb) { nome = "TELEFONE", tipo = "VARCHAR2(255)", prefixo = Campo.Prefixos.OBS });
            p.campos.Add(2608, new Campo(tb) { nome = "CELULAR", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2609, new Campo(tb) { nome = "PESSOAL", tipoPronto = Campo.TiposProntos.BOOLEAN, mapeamento1 = new string[1] { "Telefone Próprio" } });
            p.campos.Add(2610, new Campo(tb) { nome = "SMSAUTORIZADO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2611, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaEmail", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCDelete = true, gerarPCUpdate = true, comentario = "E-mails de pessoas físicas ou jurídicas" };
            p.campos.Add(2701, new Campo(tb) { identity = true });
            p.campos.Add(2702, new Campo(tb) { fk = p.campos[2101], notNull = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(2703, new Campo(tb) { tipoPronto = Campo.TiposProntos.EMAIL, notNull = true, mapeamento1 = new string[1] { "E-mail" } });
            p.campos.Add(2704, new Campo(tb) { nome = "PESSOAL", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2705, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaEndereco", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCDelete = true, gerarPCUpdate = true, comentario = "Endereços de pessoas físicas ou jurídicas" };
            p.campos.Add(2801, new Campo(tb) { identity = true });
            p.campos.Add(2802, new Campo(tb) { fk = p.campos[2101], notNull = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(2803, new Campo(tb) { nome = "CIDADE", tipo = "VARCHAR2(400)", notNull = true, prefixo = Campo.Prefixos.DES, mapeamento1 = new string[3] { "Cidade do Endereco Residencial", "Cidade do Endereco Comercial", "Cidade/UF" } });
            p.campos.Add(2804, new Campo(tb) { nome = "LOGRADOURO", tipo = "VARCHAR2(500)", notNull = true, prefixo = Campo.Prefixos.DES, mapeamento1 = new string[4] { "Logradouro", "Endereço Residencial", "Endereço Comercial", "Endereço Residencial Atual" } });
            p.campos.Add(2805, new Campo(tb) { nome = "RESIDENCIAL", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2806, new Campo(tb) { nome = "RESIDENCIA", fk = p.campos[1701], prefixo = Campo.Prefixos.TPO, mapeamento1 = new string[1] { "Tipo de Residência" } });
            p.campos.Add(2807, new Campo(tb) { nome = "RESIDENCIAATUAL", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2808, new Campo(tb) { nome = "ENDERECO", tipo = "VARCHAR2(6)", prefixo = Campo.Prefixos.NUM, mapeamento1 = new string[3] { "Número do Endereço Residencial", "Número do Endereço Comercial", "Número" } });
            p.campos.Add(2809, new Campo(tb) { nome = "BAIRRO", tipo = "VARCHAR2(99)", prefixo = Campo.Prefixos.DES, mapeamento1 = new string[3] { "Bairro do Endereço Residencial", "Bairro do Endereço Comercial", "Bairro" } });
            p.campos.Add(2810, new Campo(tb) { nome = "UF", tipo = "VARCHAR2(4)", prefixo = Campo.Prefixos.SLG, mapeamento1 = new string[2] { "UF do Endereço Residencial", "UF do Endereço Comercial" } }); //NÃO COLOCAR UFBRASIL, PODE SER ENDEREÇO ESTRANGEIRO
            p.campos.Add(2811, new Campo(tb) { nome = "CEP", tipo = "VARCHAR2(9)", prefixo = Campo.Prefixos.NUM, mapeamento1 = new string[3] { "CEP", "CEP Residencial", "CEP Comercial" } });
            p.campos.Add(2812, new Campo(tb) { nome = "COMPLEMENTO", tipo = "VARCHAR2(99)", prefixo = Campo.Prefixos.DES, mapeamento1 = new string[3] { "Complemento do Endereço Residencial", "Complemento do Endereço Comercial", "Complemento" } });
            p.campos.Add(2813, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaPatrimonio", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCDelete = true, gerarPCUpdate = true, comentario = "Dados patrimoniais de pessoa física ou jurídica" };
            p.campos.Add(2901, new Campo(tb) { identity = true });
            p.campos.Add(2902, new Campo(tb) { fk = p.campos[2101], notNull = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(2903, new Campo(tb) { nome = "ESTIMADO", tipoPronto = Campo.TiposProntos.REAIS, notNull = true, mapeamento1 = new string[6] { "Valor Automóveis", "Bens", "Valor Imóveis", "Valor Investimentos", "Valor Outros", "Outros Bens" } });
            p.campos.Add(2904, new Campo(tb) { nome = "PATRIMONIO", tipo = "VARCHAR2(256)", notNull = true, prefixo = Campo.Prefixos.DES });
            p.campos.Add(2905, new Campo(tb) { nome = "QUITADO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(2906, new Campo(tb) { nome = "IMOVEL", tipoPronto = Campo.TiposProntos.BOOLEAN, mapeamento1 = new string[1] { "Possui Bens e Imóveis" } });
            p.campos.Add(2907, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaRelativo", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCDelete = true, gerarPCUpdate = true, comentario = "Dados de parentes/terceiros relacionados, ex: filho, esposa, advogado, procurador(representante), etc" };
            p.campos.Add(3001, new Campo(tb) { identity = true });
            p.campos.Add(3002, new Campo(tb) { fk = p.campos[2101], notNull = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(3003, new Campo(tb) { nome = "RELACAO", fk = p.campos[1201], notNull = true, prefixo = Campo.Prefixos.TPO, mapeamento1 = new string[1] { "Tipo do Relacionamento" } });
            p.campos.Add(3004, new Campo(tb) { nome = "PESSOARELATIVO", tipoPronto = Campo.TiposProntos.NOME, notNull = true, mapeamento1 = new string[6] { "Nome Mãe", "Nome do Pai", "Nome Cônjuge", "Nome do Relacionado", "Nome da Referência", "Nome Representante Legal" } });
            p.campos.Add(3005, new Campo(tb) { nome = "CARGO", tipo = "VARCHAR2(255)", prefixo = Campo.Prefixos.DES, mapeamento1 = new string[2] { "Cargo do Relacionado", "Cargo" } });
            p.campos.Add(3006, new Campo(tb) { nome = "REPRESENTANTE", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(3007, new Campo(tb) { tipoPronto = Campo.TiposProntos.SEXO });
            p.campos.Add(3008, new Campo(tb) { nome = "NASCIMENTO", tipoPronto = Campo.TiposProntos.DATA });
            p.campos.Add(3009, new Campo(tb) { tipoPronto = Campo.TiposProntos.CPF, mapeamento1 = new string[3] { "CPF Representante Legal", "CPF Cônjuge", "CPF" } });
            p.campos.Add(3010, new Campo(tb) { nome = "RG", tipo = "VARCHAR2(12)", prefixo = Campo.Prefixos.NUM });
            p.campos.Add(3011, new Campo(tb) { nome = "RG_EMISSAO", tipoPronto = Campo.TiposProntos.DATA });
            p.campos.Add(3012, new Campo(tb) { nome = "RG_ORGEMISSOR", tipo = "VARCHAR2(5)", prefixo = Campo.Prefixos.SLG });
            p.campos.Add(3013, new Campo(tb) { nome = "RG_UF", tipoPronto = Campo.TiposProntos.UFBRASIL });
            p.campos.Add(3014, new Campo(tb) { tipoPronto = Campo.TiposProntos.TELEFONE, mapeamento1 = new string[1] { "Número de Telefone da Referência" } });
            p.campos.Add(3015, new Campo(tb) { tipoPronto = Campo.TiposProntos.DDD, mapeamento1 = new string[1] { "DDD da Referencia" } });
            p.campos.Add(3016, new Campo(tb) { tipoPronto = Campo.TiposProntos.RAMAL, mapeamento1 = new string[1] { "Ramal do Número de Telefone da Referência" } });
            p.campos.Add(3017, new Campo(tb) { nome = "CLIENTE", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(3018, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaDocumento", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCDelete = true, gerarPCUpdate = true, comentario = "Documentos de pessoa física ou jurídica" };
            p.campos.Add(3101, new Campo(tb) { identity = true });
            p.campos.Add(3102, new Campo(tb) { fk = p.campos[2101], notNull = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(3103, new Campo(tb) { nome = "DOCUMENTO", fk = p.campos[1301], notNull = true, prefixo = Campo.Prefixos.TPO });
            p.campos.Add(3104, new Campo(tb) { nome = "DOCUMENTO", tipo = "VARCHAR2(99)", notNull = true, prefixo = Campo.Prefixos.NUM, mapeamento1 = new string[2] { "Número RG", "Número do Documento" } });
            p.campos.Add(3105, new Campo(tb) { nome = "DOCUMENTO", tipo = "VARCHAR2(3)", prefixo = Campo.Prefixos.DIG, mapeamento1 = new string[1] { "Dígito RG" } });
            p.campos.Add(3106, new Campo(tb) { fk = p.campos[1801] }); //COD_PAIS
            p.campos.Add(3112, new Campo(tb) { nome = "UF_ORGEMISSOR", tipoPronto = Campo.TiposProntos.UFBRASIL, mapeamento1 = new string[2] { "UF Emissão RG", "UF Emissão Documento" } });
            p.campos.Add(3107, new Campo(tb) { nome = "ORGEMISSOR", tipo = "VARCHAR2(5)", prefixo = Campo.Prefixos.SLG, mapeamento1 = new string[3] { "Órgão Emissor documento", "Órgao Emissor RG", "Código Emissor" } });
            p.campos.Add(3108, new Campo(tb) { nome = "EMISSAO", tipoPronto = Campo.TiposProntos.DATA, mapeamento1 = new string[2] { "Data de Emissão RG", "Data de Emissão Documento" } });
            p.campos.Add(3109, new Campo(tb) { nome = "VALIDADE", tipoPronto = Campo.TiposProntos.DATA, mapeamento1 = new string[1] { "Data de Vencimento Documento" } });
            p.campos.Add(3113, new Campo(tb) { nome = "DOCDEIDENTIFICACAO", tipoPronto = Campo.TiposProntos.BOOLEAN, mapeamento1 = new string[1] { "Documento de Identificacão" } });
            p.campos.Add(3111, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaContaExterna", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCDelete = true, gerarPCUpdate = true, comentario = "Contas que a pessoa física ou jurídica possui fora da crefisa (itaú, bradesco, etc)" };
            p.campos.Add(3201, new Campo(tb) { identity = true });
            p.campos.Add(3202, new Campo(tb) { fk = p.campos[2101], notNull = true, chaveSel = true }); //CODPESSOA
            p.campos.Add(3203, new Campo(tb) { fk = p.campos[2001], notNull = true });
            p.campos.Add(3204, new Campo(tb) { nome = "CONTA", tipo = "VARCHAR2(9)", notNull = true, prefixo = Campo.Prefixos.NUM });
            p.campos.Add(3205, new Campo(tb) { nome = "AGENCIA", tipo = "VARCHAR2(5)", prefixo = Campo.Prefixos.NUM });
            p.campos.Add(3206, new Campo(tb) { nome = "AGENCIA", tipo = "VARCHAR2(2)", prefixo = Campo.Prefixos.DIG });
            p.campos.Add(3207, new Campo(tb) { nome = "CONTA", tipo = "VARCHAR2(2)", prefixo = Campo.Prefixos.DIG });
            p.campos.Add(3208, new Campo(tb) { nome = "CONTA", tipo = "VARCHAR2(4)", prefixo = Campo.Prefixos.TPO });
            p.campos.Add(3209, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaProduto", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém os produtos que foram ativados para a pessoa" };
            p.campos.Add(6401, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(6402, new Campo(tb) { fk = p.campos[2101] }); //CODPESSOA
            p.campos.Add(6403, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("PessoaDocDossie", apiBco, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém um dossiê com todos os documentos de uma pessoa" };
            p.campos.Add(3301, new Campo(tb) { identity = true });
            p.campos.Add(3302, new Campo(tb) { chaveSel = true, notNull = true, fk = p.campos[2101] }); //CODPESSOA
            p.campos.Add(3303, new Campo(tb) { fk = p.campos[5301] }); //COD PROCESSO ORIGEM
            p.campos.Add(3304, new Campo(tb) { fk = p.campos[5401] }); //COD SISTEMA ORIGEM
            p.campos.Add(3305, new Campo(tb) { nome = "DOCUMENTO", fk = p.campos[1301], notNull = true, prefixo = Campo.Prefixos.TPO });
            p.campos.Add(3306, new Campo(tb) { nome = "DIGITALIZACAO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(3307, new Campo(tb) { nome = "ASSINATURA", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(3308, new Campo(tb) { nome = "ENVIADO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(3309, new Campo(tb) { nome = "RECEBIDO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(3310, new Campo(tb) { nome = "RATINGBIOMETRIA", tipo = "NUMBER(9, 8)", prefixo = Campo.Prefixos.PCT });
            p.campos.Add(3311, new Campo(tb) { nome = "CAMINHOARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });
            p.campos.Add(3312, new Campo(tb) { nome = "URLARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });
            
            tb = new Tabela("Conta", apiBco, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém contas de pessoas físicas e jurídicas na crefisa" };
            p.campos.Add(3401, new Campo(tb) { identity = true });
            p.campos.Add(3402, new Campo(tb) { chaveSel = true, notNull = true, fk = p.campos[2101] }); //CODPESSOA
            p.campos.Add(3403, new Campo(tb) { nome = "CONTA", tipo = "VARCHAR2(9)", notNull = true, prefixo = Campo.Prefixos.NUM, mapeamento1 = new string[1] { "Número Conta Corrente" } });
            p.campos.Add(3404, new Campo(tb) { nome = "CONTA", tipo = "VARCHAR2(5)", prefixo = Campo.Prefixos.DIG });
            p.campos.Add(3407, new Campo(tb) { nome = "PACOTETARIFA", tipo = "INTEGER", prefixo = Campo.Prefixos.COD, mapeamento1 = new string[1] { "Tarifa" } });
            p.campos.Add(3405, new Campo(tb) { nome = "LOJA", tipo = "INTEGER", prefixo = Campo.Prefixos.COD, mapeamento1 = new string[1] { "E-mail da loja(dentro da tabela)" } });
            p.campos.Add(3406, new Campo(tb) { nome = "ATIVO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("Beneficio", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém os dados de benefícios de inss de pessoas físicas" };
            p.campos.Add(3501, new Campo(tb) { identity = true });
            p.campos.Add(3511, new Campo(tb) { chaveSel = true, notNull = true, fk = p.campos[2101] }); //CODPESSOA
            p.campos.Add(3512, new Campo(tb) { fk = p.campos[4701] }); //TIPO MEIO DE PAGAMENTO
            p.campos.Add(3502, new Campo(tb) { nome = "ORGAOPAGADOR", tipo = "INTEGER", prefixo = Campo.Prefixos.COD, mapeamento1 = new string[1] { "Órgão Pagador" } });
            p.campos.Add(3503, new Campo(tb) { nome = "ULTIMACONCESSAO", tipoPronto = Campo.TiposProntos.DATA, mapeamento1 = new string[1] { "Data Última Concessão" } });
            p.campos.Add(3504, new Campo(tb) { nome = "CONCESSAO", tipoPronto = Campo.TiposProntos.DATA, mapeamento1 = new string[1] { "Data Concessao" } });
            p.campos.Add(3505, new Campo(tb) { nome = "ESPECIE", tipo = "INTEGER", prefixo = Campo.Prefixos.TPO, mapeamento1 = new string[1] { "Tipo Especie" } });
            p.campos.Add(3506, new Campo(tb) { nome = "CONTACORRENTE", tipo = "VARCHAR2(9)", prefixo = Campo.Prefixos.NUM });
            p.campos.Add(3507, new Campo(tb) { nome = "NIT", tipo = "VARCHAR2(9)", prefixo = Campo.Prefixos.NUM, mapeamento1 = new string[1] { "Numero NIT" } });
            p.campos.Add(3508, new Campo(tb) { fk = p.campos[5201], mapeamento1 = new string[1] { "Cod status" } }); //COD STATUS SOLICITAÇÃO
            p.campos.Add(3509, new Campo(tb) { nome = "BENEFICIO", tipo = "INTEGER", unique = true, chaveSel = true, prefixo = Campo.Prefixos.NUM, mapeamento1 = new string[1] { "Número Benefício" } });
            p.campos.Add(3510, new Campo(tb) { nome = "DIAUTIL", tipo = "INTEGER", prefixo = Campo.Prefixos.COD, mapeamento1 = new string[1] { "Dia Util Liberado Pagamento" } });

            tb = new Tabela("SolicBeneficio", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém o controle de solictações de benefícios" };
            p.campos.Add(3601, new Campo(tb) { identity = true });
            p.campos.Add(3602, new Campo(tb) { nome = "SOLICITACAO", tipo = "INTEGER", prefixo = Campo.Prefixos.NUM, mapeamento1 = new string[1] { "Número Solicitacao" } });
            p.campos.Add(3603, new Campo(tb) { fk = p.campos[3509] }); // NUM BENEFÍCIO
            p.campos.Add(3604, new Campo(tb) { fk = p.campos[2101], chaveSel = true }); //CODPESSOA
            p.campos.Add(3605, new Campo(tb) { fk = p.campos[5201], mapeamento1 = new string[1] { "Código Status solicitação" } }); //COD STATUS SOLICITAÇÃO
            p.campos.Add(3606, new Campo(tb) { nome = "SOLICITACAO", tipoPronto = Campo.TiposProntos.DATA });

            tb = new Tabela("SolicBeneficioDoc", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém os documentos das solictações de benefícios de inss" };
            p.campos.Add(4901, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(4902, new Campo(tb) { fk = p.campos[3601] }); //COD_SOLIC BENEFÍCIO
            p.campos.Add(4903, new Campo(tb) { nome = "DOCUMENTO", fk = p.campos[1301], notNull = true, prefixo = Campo.Prefixos.TPO });
            p.campos.Add(4904, new Campo(tb) { nome = "DIGITALIZACAO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4905, new Campo(tb) { nome = "ASSINATURA", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4906, new Campo(tb) { nome = "ENVIADO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4907, new Campo(tb) { nome = "RECEBIDO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4908, new Campo(tb) { nome = "RATINGBIOMETRIA", tipo = "NUMBER(9, 8)", prefixo = Campo.Prefixos.PCT });
            p.campos.Add(4909, new Campo(tb) { nome = "CAMINHOARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });
            p.campos.Add(4910, new Campo(tb) { nome = "URLARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });

            tb = new Tabela("SolicAltFormaPgto", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCDelete = true, gerarPCUpdate = true, comentario = "Contém o controle de solictações de alteração de forma de pagamento de benefício inss" };
            p.campos.Add(3701, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(3702, new Campo(tb) { fk = p.campos[3509] }); // NUM BENEFÍCIO
            p.campos.Add(3703, new Campo(tb) { fk = p.campos[5201]}); //COD STATUS SOLICITAÇÃO
            p.campos.Add(3704, new Campo(tb) { nome = "SOLICITACAO", tipoPronto = Campo.TiposProntos.DATA });

            tb = new Tabela("SolicAltFormPgDoc", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true , comentario = "Contém os documentos de solictações de alteração de forma de pagamento do benefício inss" };
            p.campos.Add(3801, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(3802, new Campo(tb) { fk = p.campos[3701] }); //COD_SOLIC ALTER FORMA PGTO
            p.campos.Add(3803, new Campo(tb) { nome = "DOCUMENTO", fk = p.campos[1301], notNull = true, prefixo = Campo.Prefixos.TPO });
            p.campos.Add(3804, new Campo(tb) { nome = "DIGITALIZACAO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(3805, new Campo(tb) { nome = "ASSINATURA", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(3806, new Campo(tb) { nome = "ENVIADO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(3807, new Campo(tb) { nome = "RECEBIDO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(3808, new Campo(tb) { nome = "RATINGBIOMETRIA", tipo = "NUMBER(9, 8)", prefixo = Campo.Prefixos.PCT });
            p.campos.Add(3809, new Campo(tb) { nome = "CAMINHOARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });
            p.campos.Add(3810, new Campo(tb) { nome = "URLARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });

            tb = new Tabela("SolicRecadCenso", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém as solictações de recadastramento de censo" };
            p.campos.Add(3901, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(3902, new Campo(tb) { fk = p.campos[3509] }); // NUM BENEFÍCIO
            p.campos.Add(3903, new Campo(tb) { fk = p.campos[5201] }); //COD STATUS SOLICITAÇÃO
            p.campos.Add(3904, new Campo(tb) { nome = "SOLICITACAO", tipoPronto = Campo.TiposProntos.DATA });

            tb = new Tabela("SolicRecadCensoDoc", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém os documentos de solictações de recadastramento de censo" };
            p.campos.Add(4001, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(4002, new Campo(tb) { fk = p.campos[3901] }); //COD_SOLIC RECADASTRA CENSO
            p.campos.Add(4003, new Campo(tb) { nome = "DOCUMENTO", fk = p.campos[1301], notNull = true, prefixo = Campo.Prefixos.TPO });
            p.campos.Add(4004, new Campo(tb) { nome = "DIGITALIZACAO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4005, new Campo(tb) { nome = "ASSINATURA", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4006, new Campo(tb) { nome = "ENVIADO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4007, new Campo(tb) { nome = "RECEBIDO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4008, new Campo(tb) { nome = "RATINGBIOMETRIA", tipo = "NUMBER(9, 8)", prefixo = Campo.Prefixos.PCT });
            p.campos.Add(4009, new Campo(tb) { nome = "CAMINHOARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });
            p.campos.Add(4010, new Campo(tb) { nome = "URLARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });

            tb = new Tabela("SolicConta", apiBco, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém soclitações de abertura de conta" };
            p.campos.Add(4101, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(4102, new Campo(tb) { fk = p.campos[3509] }); // NUM BENEFÍCIO
            p.campos.Add(4103, new Campo(tb) { fk = p.campos[2101] }); //CODPESSOA
            p.campos.Add(4104, new Campo(tb) { nome = "AGENCIA", tipo = "VARCHAR(5)", prefixo = Campo.Prefixos.COD });
            p.campos.Add(4105, new Campo(tb) { nome = "CONTA", tipo = "INTEGER", prefixo = Campo.Prefixos.TPO });
            p.campos.Add(4106, new Campo(tb) { nome = "INICIO_RELACIONAMENTO", tipoPronto = Campo.TiposProntos.DATA });
            p.campos.Add(4107, new Campo(tb) { nome = "FIM_RELACIONAMENTO", tipoPronto = Campo.TiposProntos.DATA });
            p.campos.Add(4108, new Campo(tb) { fk = p.campos[5201] }); //COD STATUS SOLICITAÇÃO
            p.campos.Add(4109, new Campo(tb) { nome = "SOLICITACAO", tipoPronto = Campo.TiposProntos.DATA });

            tb = new Tabela("SolicContaDoc", apiBco, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém documentos de solictações de abertura de conta" };
            p.campos.Add(4801, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(4802, new Campo(tb) { fk = p.campos[4101] }); //COD_SOLIC CONTA
            p.campos.Add(4803, new Campo(tb) { nome = "DOCUMENTO", fk = p.campos[1301], notNull = true, prefixo = Campo.Prefixos.TPO });
            p.campos.Add(4804, new Campo(tb) { nome = "DIGITALIZACAO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4805, new Campo(tb) { nome = "ASSINATURA", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4806, new Campo(tb) { nome = "ENVIADO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4807, new Campo(tb) { nome = "RECEBIDO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(4808, new Campo(tb) { nome = "RATINGBIOMETRIA", tipo = "NUMBER(9, 8)", prefixo = Campo.Prefixos.PCT });
            p.campos.Add(4809, new Campo(tb) { nome = "CAMINHOARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });
            p.campos.Add(4810, new Campo(tb) { nome = "URLARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });

            tb = new Tabela("SolicProvaVida", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém um controle de solictações de prova de vida" };
            p.campos.Add(4201, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(4202, new Campo(tb) { fk = p.campos[3509] }); // NUM BENEFÍCIO
            p.campos.Add(4203, new Campo(tb) { fk = p.campos[2101] }); //CODPESSOA
            p.campos.Add(4204, new Campo(tb) { fk = p.campos[5201] }); //COD STATUS SOLICITAÇÃO
            p.campos.Add(4205, new Campo(tb) { nome = "SOLICITACAO", tipoPronto = Campo.TiposProntos.DATA });

            tb = new Tabela("SolicProvaVidaDoc", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém documentos de solictações de prova de vida" };
            p.campos.Add(5001, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(5002, new Campo(tb) { fk = p.campos[4201] }); //COD_SOLIC PROVA VIDA
            p.campos.Add(5003, new Campo(tb) { nome = "DOCUMENTO", fk = p.campos[1301], notNull = true, prefixo = Campo.Prefixos.TPO });
            p.campos.Add(5004, new Campo(tb) { nome = "DIGITALIZACAO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(5005, new Campo(tb) { nome = "ASSINATURA", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(5006, new Campo(tb) { nome = "ENVIADO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(5007, new Campo(tb) { nome = "RECEBIDO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(5008, new Campo(tb) { nome = "RATINGBIOMETRIA", tipo = "NUMBER(9, 8)", prefixo = Campo.Prefixos.PCT });
            p.campos.Add(5009, new Campo(tb) { nome = "CAMINHOARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });
            p.campos.Add(5010, new Campo(tb) { nome = "URLARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });

            tb = new Tabela("SolicAntecipRenda", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém solictações de antecipação de renda" };
            p.campos.Add(4301, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(4302, new Campo(tb) { fk = p.campos[3509] }); // NUM BENEFÍCIO
            p.campos.Add(4303, new Campo(tb) { fk = p.campos[2101] }); //CODPESSOA
            p.campos.Add(4304, new Campo(tb) { fk = p.campos[5201] }); //COD STATUS SOLICITAÇÃO
            p.campos.Add(4305, new Campo(tb) { nome = "SOLICITACAO", tipoPronto = Campo.TiposProntos.DATA });

            tb = new Tabela("SolicAntecRendDoc", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém documentos de solictações de antecipação de renda" };
            p.campos.Add(5101, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(5102, new Campo(tb) { fk = p.campos[4301] }); //COD_SOLIC ANTECIPAÇÃO RENDA
            p.campos.Add(5103, new Campo(tb) { nome = "DOCUMENTO", fk = p.campos[1301], notNull = true, prefixo = Campo.Prefixos.TPO });
            p.campos.Add(5104, new Campo(tb) { nome = "DIGITALIZACAO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(5105, new Campo(tb) { nome = "ASSINATURA", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(5106, new Campo(tb) { nome = "ENVIADO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(5107, new Campo(tb) { nome = "RECEBIDO", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(5108, new Campo(tb) { nome = "RATINGBIOMETRIA", tipo = "NUMBER(9, 8)", prefixo = Campo.Prefixos.PCT });
            p.campos.Add(5109, new Campo(tb) { nome = "CAMINHOARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });
            p.campos.Add(5110, new Campo(tb) { nome = "URLARQUIVO", tipo = "VARCHAR2(500)", prefixo = Campo.Prefixos.DES });

            tb = new Tabela("CampoObrigatorio", apiCad, schCuc) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém as configurações de quais campos são obrigatórios para cada grupo de sistemas" };
            p.campos.Add(4501, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(4502, new Campo(tb) { fk = p.campos[4401] }); // COD_GRUPO CAMPO OBRIGATÓRIO
            p.campos.Add(4503, new Campo(tb) { nome = "Tabela", tipo = "VARCHAR2(30)", notNull = true, prefixo = Campo.Prefixos.NOM });
            p.campos.Add(4504, new Campo(tb) { nome = "Campo", tipo = "VARCHAR2(30)", notNull = true, prefixo = Campo.Prefixos.NOM });

            tb = new Tabela("Procuradores", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém os dados recebidos de procuradores" };
            p.campos.Add(4601, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(4602, new Campo(tb) { fk = p.campos[2101] }); //CODPESSOA
            p.campos.Add(4603, new Campo(tb) { tipoPronto = Campo.TiposProntos.CPF });
            p.campos.Add(4604, new Campo(tb) { nome = "PROCURADOR", tipoPronto = Campo.TiposProntos.NOME });
            p.campos.Add(4605, new Campo(tb) { tipoPronto = Campo.TiposProntos.TELEFONE });

            tb = new Tabela("Log", apiLog, schCuc) {gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém o log de alterações das tabelas do mês atual, a cada mês os dados são movidos para a tabela de hitórico de logs" };
            p.campos.Add(6001, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(6002, new Campo(tb) { nome = "TABELA", tipo = "VARCHAR2(30)", prefixo = Campo.Prefixos.NOM, notNull = true, comentario = "nome da tabela a qual pertence o registro" });
            p.campos.Add(6003, new Campo(tb) { nome = "REGISTRO", tipo = "INTEGER", prefixo = Campo.Prefixos.COD, notNull = true , comentario = "valor do campo que é chave primária da tabela, para identificar exatamente a qual registro pertence este log"} );
            p.campos.Add(6004, new Campo(tb) { nome = "INSERT", tipoPronto = Campo.TiposProntos.DATAHORA, comentario = "log da data e hora em que o registro foi criado" });
            p.campos.Add(6005, new Campo(tb) { nome = "UPDATE", tipoPronto = Campo.TiposProntos.DATAHORA, comentario = "log da data e hora em que o registro recebeu a última atualização" });
            p.campos.Add(6006, new Campo(tb) { nome = "DELETE", tipoPronto = Campo.TiposProntos.DATAHORA, comentario = "log da data e hora em que o registro foi excluído" });
            p.campos.Add(6007, new Campo(tb) { nome = "USERINSERT", tipo = "INTEGER", prefixo = Campo.Prefixos.COD, comentario = "código do usuário que efetuou a criação do registro" });
            p.campos.Add(6008, new Campo(tb) { nome = "USERUPDATE", tipo = "INTEGER", prefixo = Campo.Prefixos.COD, comentario = "código do usuário que efetuou a última atualização do registro" });
            p.campos.Add(6009, new Campo(tb) { nome = "USERDELETE", tipo = "INTEGER", prefixo = Campo.Prefixos.COD, comentario = "código do usuário que efetuou a exclusão do registro" });
            p.campos.Add(6010, new Campo(tb) { nome = "ORIGINAL", tipo = "CLOB", prefixo = Campo.Prefixos.VLR, comentario = "conteúdo dos campos do registro no momento em que foi criado" });
            p.campos.Add(6011, new Campo(tb) { nome = "ATUAL", tipo = "CLOB", prefixo = Campo.Prefixos.VLR, comentario = "conteúdo atual dos campos do registro" });
            p.campos.Add(6012, new Campo(tb) { nome = "DELETE", tipo = "CLOB", prefixo = Campo.Prefixos.VLR, comentario = "conteúdo do registro antes de ser deletado" });
            p.campos.Add(6013, new Campo(tb) { nome = "PROCESSOORIGEM", tipo = "VARCHAR2(99)", prefixo = Campo.Prefixos.NOM, comentario = "nome do processo que deu origem ao registro" });
            p.campos.Add(6014, new Campo(tb) { nome = "SISTEMAORIGEM", tipo = "VARCHAR2(99)", prefixo = Campo.Prefixos.NOM, comentario = "nome do sistema que deu origem ao registro" });

            tb = new Tabela("LogHist", apiLog, schCuc) { gerarController = false, gerarPCInsert = true, comentario = "Contém o histórico de cada mês dos logs de alterações das tabelas" };
            p.campos.Add(6501, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(6502, new Campo(tb) { nome = "AAAAMM", tipo = "VARCHAR2(6)", notNull = true, chaveSel = true, prefixo = Campo.Prefixos.VLR });
            p.campos.Add(6503, new Campo(tb) { nome = "LOG", tipo = "INTEGER", notNull = true, chaveSel = true, prefixo = Campo.Prefixos.COD });
            p.campos.Add(6504, new Campo(tb) { nome = "TABELA", tipo = "VARCHAR2(30)", prefixo = Campo.Prefixos.NOM, notNull = true });
            p.campos.Add(6505, new Campo(tb) { nome = "REGISTRO", tipo = "INTEGER", prefixo = Campo.Prefixos.COD, notNull = true });
            p.campos.Add(6506, new Campo(tb) { nome = "INSERT", tipoPronto = Campo.TiposProntos.DATAHORA });
            p.campos.Add(6507, new Campo(tb) { nome = "UPDATE", tipoPronto = Campo.TiposProntos.DATAHORA });
            p.campos.Add(6508, new Campo(tb) { nome = "DELETE", tipoPronto = Campo.TiposProntos.DATAHORA });
            p.campos.Add(6509, new Campo(tb) { nome = "USERINSERT", tipo = "INTEGER", prefixo = Campo.Prefixos.COD });
            p.campos.Add(6510, new Campo(tb) { nome = "USERUPDATE", tipo = "INTEGER", prefixo = Campo.Prefixos.COD });
            p.campos.Add(6511, new Campo(tb) { nome = "USERDELETE", tipo = "INTEGER", prefixo = Campo.Prefixos.COD });
            p.campos.Add(6512, new Campo(tb) { nome = "ORIGINAL", tipo = "CLOB", prefixo = Campo.Prefixos.VLR });
            p.campos.Add(6513, new Campo(tb) { nome = "ATUAL", tipo = "CLOB", prefixo = Campo.Prefixos.VLR });
            p.campos.Add(6514, new Campo(tb) { nome = "DELETE", tipo = "CLOB", prefixo = Campo.Prefixos.VLR });
            p.campos.Add(6515, new Campo(tb) { nome = "PROCESSOORIGEM", tipo = "VARCHAR2(99)", prefixo = Campo.Prefixos.NOM });
            p.campos.Add(6516, new Campo(tb) { nome = "SISTEMAORIGEM", tipo = "VARCHAR2(99)", prefixo = Campo.Prefixos.NOM });

            tb = new Tabela("BeneficioPgto", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém os pagamentos de benefício inss" };
            p.campos.Add(6101, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(6102, new Campo(tb) { fk = p.campos[3501] }); // COD BENEFICIO
            p.campos.Add(6103, new Campo(tb) { fk = p.campos[4701] }); // COD TIPO MEIO PAGAMENTO
            p.campos.Add(6104, new Campo(tb) { nome = "MODALIDADECONTA", tipo = "VARCHAR2(20)", prefixo = Campo.Prefixos.COD });
            p.campos.Add(6105, new Campo(tb) { nome = "CONTA", tipo = "INTEGER", prefixo = Campo.Prefixos.NUM });
            p.campos.Add(6106, new Campo(tb) { nome = "IDCONTACARTAO", tipo = "INTEGER", prefixo = Campo.Prefixos.NUM });
            p.campos.Add(6107, new Campo(tb) { nome = "PRINCIPAL", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(6108, new Campo(tb) { nome = "INTEGRADO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            tb = new Tabela("SolicBeneficioPgto", apiInss, schNfcBanco) { gerarPCSelect = true, gerarPCInsert = true, gerarPCUpdate = true, comentario = "Contém as solicitações de pagamentos de benefício inss" };
            p.campos.Add(6201, new Campo(tb) { identity = true, chaveSel = true });
            p.campos.Add(6202, new Campo(tb) { fk = p.campos[3601] }); // COD SOLIC BENEFICIO
            p.campos.Add(6203, new Campo(tb) { fk = p.campos[4701] }); // COD TIPO MEIO PAGAMENTO
            p.campos.Add(6204, new Campo(tb) { nome = "CONTA", tipo = "INTEGER", prefixo = Campo.Prefixos.NUM });
            p.campos.Add(6205, new Campo(tb) { nome = "IDCONTACARTAO", tipo = "INTEGER", prefixo = Campo.Prefixos.NUM });
            p.campos.Add(6206, new Campo(tb) { nome = "PRINCIPAL", tipoPronto = Campo.TiposProntos.BOOLEAN });
            p.campos.Add(6207, new Campo(tb) { nome = "VALIDADO", tipoPronto = Campo.TiposProntos.BOOLEAN });

            return p.CriarProjeto();
        }

    }
}
